#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "iostream"
#include <iomanip>
#include "TString.h"
#include "TCut.h"
#include "TChain.h"
#include "TH1F.h"
#include "TLegend.h"
#include <TStyle.h>
#include "RooRealVar.h"
#include "RooHypatia2.h"
#include "RooDataSet.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooAbsReal.h"
#include "RooHist.h"
#include "TF1.h"
#include "TPaveText.h"
#include "TText.h"
#include "RooAddPdf.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooJohnson.h"
#include "/home/wanghj/lhcb_style.h"
#include "RooCFunction1Binding.h"

#include "../cut.h"
using namespace std;
using namespace RooFit;
void getPDF_beforeBDT(string infile, TString txtfile, TString pdffile, TString initfile){
		setStyle_lhcb();
		if(infile.find("Lcpi") != std::string::npos){
				int Nbin = 80;
				TChain *tc = new TChain("DecayTree");
				tc->Add(infile.c_str());
				TTree *t = tc->CopyTree(cut_Lc+cut_loose_Lb, "", 1000000);
				cout << t -> GetEntries();

				RooRealVar *m_Lb = new RooRealVar("DTFonlyPV_Lb_M", "M(#it{#Lambda_{c}^{+}#pi^{-}}) (MeV/#it{c}^{2})", 5520, 5850);

				RooRealVar *Lb_lambda = new RooRealVar("Lb_lambda", "Lb_lambda", -5.0,-10.0,0.0);
				RooRealVar *Lb_zeta   = new RooRealVar("Lb_zeta",   "Lb_zeta", 0);
				RooRealVar *Lb_beta   = new RooRealVar("Lb_beta",   "Lb_beta",  0, -5, 5);
				RooRealVar *Lb_sigma  = new RooRealVar("Lb_sigma",  "Lb_sigma", 10, 0, 100);
				RooRealVar *Lb_mean   = new RooRealVar("Lb_mean",   "Lb_mean", 5619, 5600, 5640);
				RooRealVar *Lb_a      = new RooRealVar("Lb_a",      "Lb_a", 1.0, 0, 3); 
				RooRealVar *Lb_n      = new RooRealVar("Lb_n",      "Lb_n", 1.0, 0, 3);
				RooRealVar *Lb_a2     = new RooRealVar("Lb_a2",     "Lb_a2", 1.0, 0, 3); 
				RooRealVar *Lb_n2     = new RooRealVar("Lb_n2",     "Lb_n2", 1.0, 0, 3);
				RooHypatia2	*Hypatia2 = new RooHypatia2("Hypatia2","Hypatia2", *m_Lb,*Lb_lambda,*Lb_zeta,*Lb_beta,*Lb_sigma,*Lb_mean,*Lb_a,*Lb_n,*Lb_a2,*Lb_n2);

				//Read initial params
				auto h_params = Hypatia2 -> getParameters(*m_Lb);
				h_params->readFromFile(initfile);
				h_params->Print("v");

				//Poly. for comb bkg

				RooRealVar *n_signal =   new RooRealVar("n_signal", "n_signal", 5e4, 0, 20e4);

				RooAddPdf *model = new RooAddPdf("model", "model", 
								RooArgList(*Hypatia2),
								RooArgList(*n_signal));

				RooDataSet *dataset = new RooDataSet("dataset", "dataset", t, *m_Lb);
				//RooDataHist *dataset = new RooDataHist("dataset", "dataset",  *m_Lb, h_data);

				//Fit
				auto r = model-> fitTo(*dataset, Save(), Extended());
				r -> Print("v");

				//Plot
				auto frame = m_Lb -> frame(5520, 5850, Nbin);
				frame->GetYaxis()->SetTitle("Events / (5.0 MeV/#it{c}^{2})");
				frame->GetXaxis()->SetTitleFont(22);
				frame->GetYaxis()->SetTitleFont(22);
				frame->GetXaxis()->SetLabelFont(22);
				frame->GetYaxis()->SetLabelFont(22);
				frame->GetXaxis()->SetTitleOffset(1.00);

				dataset ->    plotOn(frame, Name("mc"),DataError(RooAbsData::Poisson));
				model -> plotOn(frame, Name("sum"),    LineStyle(1), LineColor(kSpring-6));
				RooHist *pull = frame -> pullHist();
				//model -> plotOn(frame, Components("Hypatia2"), Name("Hypatia2"),    LineStyle(2), LineColor(kRed));

				//Pull
				auto frame_pull = m_Lb -> frame(5520, 5850, Nbin);
				/*
					 TF1 *       f_zero = new TF1("f_zero", "0", 0, 1);
					 RooAbsReal *F_zero = RooFit::bindFunction(f_zero, *m_Lb); // a y=0 line
				//F_zero->plotOn(frame_pull, LineStyle(2), LineWidth(2), LineColor(kBlue));

				TF1 *       f_up = new TF1("f_zero", "3", 0, 1);
				RooAbsReal *F_up = RooFit::bindFunction(f_up, *m_Lb); // a y=0 line
				F_up->plotOn(frame_pull, LineStyle(2), LineWidth(2), LineColor(kAzure+4));

				TF1 *       f_down = new TF1("f_zero", "-3", 0, 1);
				RooAbsReal *F_down = RooFit::bindFunction(f_down, *m_Lb); // a y=0 line
				F_down->plotOn(frame_pull, LineStyle(2), LineWidth(2), LineColor(kAzure+4));
				*/

				RooConstVar F_zero("F_zero", "F_zero", 0);
				RooConstVar F_up("F_up", "F_up", 3);
				RooConstVar F_down("F_down", "F_down", -3);

				F_zero.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kBlue));
				F_up.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));
				F_down.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));

				frame_pull->addPlotable(pull, "P");

				TCanvas *cs = new TCanvas("cs", "cs", 800, 600);
				cs -> Divide(1,2);
				cs->SetLeftMargin(0.01);
				cs->SetRightMargin(0.01);
				cs->SetTopMargin(0.01);
				cs->SetBottomMargin(0.0);

				cs -> cd(1) -> SetPad(0.0, 0.0, 1.0, 0.25);
				gPad->SetTopMargin(0.00);
				gPad->SetBottomMargin(0.60);
				gPad->SetLeftMargin(0.12);
				//gPad->SetLogy();
				frame_pull->SetMaximum(7);
				frame_pull->SetMinimum(-7);
				frame_pull->GetYaxis()->SetNdivisions(104);
				frame_pull->GetYaxis()->SetLabelSize(0.15);    
				frame_pull->GetYaxis()->CenterTitle(true);
				frame_pull->GetYaxis()->SetTitle("Pull");
				frame_pull->GetYaxis()->SetTitleSize(0.20);
				frame_pull->GetYaxis()->SetTitleOffset(0.29);
				frame_pull->GetXaxis()->SetNdivisions(505);
				frame_pull->GetXaxis()->SetLabelSize(0.2);
				//frame_pull->GetXaxis()->CenterTitle(true);
				frame_pull->GetXaxis()->SetTitleSize(0.22);
				frame_pull->GetXaxis()->SetTitleOffset(0.85);
				frame_pull->GetXaxis()->SetTitleFont(22);
				frame_pull->GetYaxis()->SetTitleFont(22);
				frame_pull->GetXaxis()->SetLabelFont(22);
				frame_pull->GetYaxis()->SetLabelFont(22);
				frame_pull->Draw();


				cs -> cd(2) -> SetPad(0.0, 0.25, 1.0, 1.0);
				gPad->SetTopMargin(0.02);
				gPad->SetBottomMargin(0.00);
				gPad->SetLeftMargin(0.12);
				frame->GetYaxis()->SetMaxDigits(5);
				frame->GetYaxis()->SetTitleOffset(0.8);
				frame->GetYaxis()->CenterTitle(true);
				frame->GetXaxis()->SetTitleSize(1.2);
        frame->SetMaximum(15000);
				frame->SetMinimum(0.0001);
				frame->Draw();

				TPaveText *pt = new TPaveText(0.5964912,0.1041667,0.8508772,0.2453704,"BRNDC");
				pt->SetBorderSize(0);
				pt->SetFillColor(10);
				pt->SetFillStyle(1001);
				pt->SetTextAlign(12);
				pt->SetTextSize(0.06);
				pt->SetTextFont(22);
				TText *text;
				text = pt->AddText("#it{#Lambda_{b}^{0}#rightarrow#Lambda_{c}^{+}#pi^{-}, #Lambda_{c}^{+}#rightarrowp#pi^{+}#pi^{-}}");
				pt->Draw("same");
				TLegend *leg = new TLegend(0.1660401,0.599537,0.4204261,0.9305556,NULL,"brNDC");
				leg->AddEntry(frame -> findObject("mc"), "MC", "lep");
				leg->AddEntry(frame -> findObject("sum"), "Hypatia2", "l");
				leg->SetFillStyle(1001);
				leg->SetTextSize(0.06);
				leg->SetBorderSize(0);
				leg->SetTextFont(22);
				leg->Draw("same");

				TPaveText *pt1 = new TPaveText(0.5902256,0.6018519,0.8972431,0.9328704,"BRNDC");
				pt1->SetBorderSize(0);
				pt1->SetFillColor(10);
				pt1->SetFillStyle(1001);
				pt1->SetTextAlign(12);
				pt1->SetTextSize(0.06);
				pt1->SetTextFont(22);
				TString str1=Form("mean=%1.2f#pm%1.2f",Lb_mean->getVal(),Lb_mean->getError());
				TString str2=Form("sigma=%1.2f#pm%1.2f",Lb_sigma->getVal(),Lb_sigma->getError());
				TString str3=Form("N_{sig}=%1.2f#pm%1.2f",n_signal->getVal(),n_signal->getError());

				TText *text1;
				text1 = pt1->AddText(str1);
				text1 = pt1->AddText(str2);
				text1 = pt1->AddText(str3);
				pt1->Draw("same");

				auto params = model -> getParameters(*m_Lb);
			  Lb_lambda -> setConstant();
			  Lb_zeta   -> setConstant();
			  Lb_beta   -> setConstant();
			  Lb_a      -> setConstant();
			  Lb_n      -> setConstant();
			  Lb_a2     -> setConstant();
			  Lb_n2     -> setConstant();
				params->writeToFile(txtfile);

				cs->Update();
				cs->Print(pdffile);
		}
		else{
				int Nbin = 80;
				TChain *tc = new TChain("DecayTree");
				tc->Add(infile.c_str());
				TTree *t = tc->CopyTree(cut_Lc+cut_loose_Lb, "", 1000000);
				cout << t -> GetEntries();

				RooRealVar *m_Lb = new RooRealVar("DTFonlyPV_Lb_M", "M(#it{#Lambda_{c}^{+}#pi^{-}}) (MeV/#it{c}^{2})", 5520, 5850);

				RooRealVar  *LcK_mean = new RooRealVar("LcK_mean", "LcK_mean", 5619, 5570, 5640);
				RooRealVar  *LcK_sigma  = new RooRealVar("LcK_sigma", "LcK_sigma", 10, 0, 500);
				RooGaussian *gaussian=new RooGaussian("gaussian","gaussian",*m_Lb,*LcK_mean,*LcK_sigma);

				RooRealVar  *LcK_lambda = new RooRealVar("LcK_lambda", "LcK_lambda", 10, 0, 500);
				RooRealVar  *LcK_gamma  = new RooRealVar("LcK_gamma", "LcK_gamma", -0.21, -30, 30);
				RooRealVar  *LcK_delta  = new RooRealVar("LcK_delta", "LcK_delta",  1, 0, 10);
				RooJohnson  *johnson = new RooJohnson("johnson", "johnson", *m_Lb, *LcK_mean, *LcK_lambda, *LcK_gamma, *LcK_delta);

				RooRealVar *LcK_frac_john = new RooRealVar("LcK_frac_john", "LcK_frac_john", 0.2, 0, 1);
				RooAddPdf  *model0 = new RooAddPdf("model0", "model0", RooArgList(*gaussian, *johnson), RooArgList(*LcK_frac_john));

				//Read initial params
				auto h_params = model0 -> getParameters(*m_Lb);
				h_params->readFromFile(initfile);
				h_params->Print("v");

				//Poly. for comb bkg
				RooRealVar *n_signal =   new RooRealVar("n_signal", "n_signal", 5e4, 0, 20e4);

				RooAddPdf *model = new RooAddPdf("model", "model", 
								RooArgList(*model0),
								RooArgList(*n_signal));

				RooDataSet *dataset = new RooDataSet("dataset", "dataset", t, *m_Lb);
				//RooDataHist *dataset = new RooDataHist("dataset", "dataset",  *m_Lb, h_data);

				//Fit
				auto r = model-> fitTo(*dataset, Save(), Extended());
				r -> Print("v");

				//Plot
				auto frame = m_Lb -> frame(5520, 5850, Nbin);
				frame->GetYaxis()->SetTitle("Events / (5.0 MeV/#it{c}^{2})");
				frame->GetXaxis()->SetTitleFont(22);
				frame->GetYaxis()->SetTitleFont(22);
				frame->GetXaxis()->SetLabelFont(22);
				frame->GetYaxis()->SetLabelFont(22);
				frame->GetXaxis()->SetTitleOffset(1.00);

				dataset ->    plotOn(frame, Name("mc"),DataError(RooAbsData::Poisson));
				model -> plotOn(frame, Name("sum"),    LineStyle(1), LineColor(kSpring-6));
				RooHist *pull = frame -> pullHist();
				model -> plotOn(frame, Components("johnson"), Name("johnson"),    LineStyle(2), LineColor(kMagenta-2));
				model -> plotOn(frame, Components("gaussian"), Name("gaussian"),  LineStyle(2), LineColor(kOrange+6));
				//model -> plotOn(frame, Components("Hypatia2"), Name("Hypatia2"),    LineStyle(2), LineColor(kRed));

				//Pull
				auto frame_pull = m_Lb -> frame(5520, 5850, Nbin);
				/*
					 TF1 *       f_zero = new TF1("f_zero", "0", 0, 1);
					 RooAbsReal *F_zero = RooFit::bindFunction(f_zero, *m_Lb); // a y=0 line
				//F_zero->plotOn(frame_pull, LineStyle(2), LineWidth(2), LineColor(kBlue));

				TF1 *       f_up = new TF1("f_zero", "3", 0, 1);
				RooAbsReal *F_up = RooFit::bindFunction(f_up, *m_Lb); // a y=0 line
				F_up->plotOn(frame_pull, LineStyle(2), LineWidth(2), LineColor(kAzure+4));

				TF1 *       f_down = new TF1("f_zero", "-3", 0, 1);
				RooAbsReal *F_down = RooFit::bindFunction(f_down, *m_Lb); // a y=0 line
				F_down->plotOn(frame_pull, LineStyle(2), LineWidth(2), LineColor(kAzure+4));
				*/

				RooConstVar F_zero("F_zero", "F_zero", 0);
				RooConstVar F_up("F_up", "F_up", 3);
				RooConstVar F_down("F_down", "F_down", -3);

				F_zero.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kBlue));
				F_up.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));
				F_down.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));

				frame_pull->addPlotable(pull, "P");

				TCanvas *cs = new TCanvas("cs", "cs", 800, 600);
				cs -> Divide(1,2);
				cs->SetLeftMargin(0.01);
				cs->SetRightMargin(0.01);
				cs->SetTopMargin(0.01);
				cs->SetBottomMargin(0.0);

				cs -> cd(1) -> SetPad(0.0, 0.0, 1.0, 0.25);
				gPad->SetTopMargin(0.00);
				gPad->SetBottomMargin(0.60);
				gPad->SetLeftMargin(0.12);
				//gPad->SetLogy();
				frame_pull->SetMaximum(7);
				frame_pull->SetMinimum(-7);
				frame_pull->GetYaxis()->SetNdivisions(104);
				frame_pull->GetYaxis()->SetLabelSize(0.15);    
				frame_pull->GetYaxis()->CenterTitle(true);
				frame_pull->GetYaxis()->SetTitle("Pull");
				frame_pull->GetYaxis()->SetTitleSize(0.20);
				frame_pull->GetYaxis()->SetTitleOffset(0.29);
				frame_pull->GetXaxis()->SetNdivisions(505);
				frame_pull->GetXaxis()->SetLabelSize(0.2);
				//frame_pull->GetXaxis()->CenterTitle(true);
				frame_pull->GetXaxis()->SetTitleSize(0.22);
				frame_pull->GetXaxis()->SetTitleOffset(0.85);
				frame_pull->GetXaxis()->SetTitleFont(22);
				frame_pull->GetYaxis()->SetTitleFont(22);
				frame_pull->GetXaxis()->SetLabelFont(22);
				frame_pull->GetYaxis()->SetLabelFont(22);
				frame_pull->Draw();


				cs -> cd(2) -> SetPad(0.0, 0.25, 1.0, 1.0);
				gPad->SetTopMargin(0.02);
				gPad->SetBottomMargin(0.00);
				gPad->SetLeftMargin(0.12);
				frame->GetYaxis()->SetMaxDigits(5);
				frame->GetYaxis()->SetTitleOffset(0.8);
				frame->GetYaxis()->CenterTitle(true);
				frame->GetXaxis()->SetTitleSize(1.2);
				frame->SetMinimum(0.0001);
				frame->SetMaximum(1800);
				frame->Draw();

				TPaveText *pt = new TPaveText(0.5964912,0.1041667,0.8508772,0.2453704,"BRNDC");
				pt->SetBorderSize(0);
				pt->SetFillColor(10);
				pt->SetFillStyle(1001);
				pt->SetTextAlign(12);
				pt->SetTextSize(0.06);
				pt->SetTextFont(22);
				TText *text;
				text = pt->AddText("#it{#Lambda_{b}^{0}#rightarrow#Lambda_{c}^{+}K^{-}, #Lambda_{c}^{+}#rightarrowp#pi^{+}#pi^{-}}");
				pt->Draw("same");
				TLegend *leg = new TLegend(0.1660401,0.599537,0.4204261,0.9305556,NULL,"brNDC");
				leg->AddEntry(frame -> findObject("mc"), "MC", "lep");
				leg->AddEntry(frame -> findObject("sum"), "Fit result", "l");
				leg->AddEntry(frame -> findObject("johnson"), "Johnson", "l");
				leg->AddEntry(frame -> findObject("gaussian"), "Gaussian", "l");
				leg->SetFillStyle(1001);
				leg->SetTextSize(0.06);
				leg->SetBorderSize(0);
				leg->SetTextFont(22);
				leg->Draw("same");

				TPaveText *pt1 = new TPaveText(0.5902256,0.6018519,0.8972431,0.9328704,"BRNDC");
				pt1->SetBorderSize(0);
				pt1->SetFillColor(10);
				pt1->SetFillStyle(1001);
				pt1->SetTextAlign(12);
				pt1->SetTextSize(0.06);
				pt1->SetTextFont(22);
				TString str1=Form("mean=%1.2f#pm%1.2f",LcK_mean->getVal(),LcK_mean->getError());
				TString str2=Form("sigma=%1.2f#pm%1.2f",LcK_sigma->getVal(),LcK_sigma->getError());
				TString str3=Form("N_{sig}=%1.2f#pm%1.2f",n_signal->getVal(),n_signal->getError());

				TText *text1;
				text1 = pt1->AddText(str1);
				text1 = pt1->AddText(str2);
				text1 = pt1->AddText(str3);
				pt1->Draw("same");

				auto params = model -> getParameters(*m_Lb);
			  LcK_frac_john -> setConstant();
			  LcK_lambda    -> setConstant();
			  LcK_gamma     -> setConstant();
			  LcK_delta     -> setConstant();
				params->writeToFile(txtfile);

				cs->Update();
				cs->Print(pdffile);
		}
}

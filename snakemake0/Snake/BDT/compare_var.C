#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"                                                       
#include "/home/wangziyi/usefull/Draw_Style/lhcbStyle.h"
#include "/publicfs2/ucas/user/niuqile/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake0/Snake/cut.h"

void compare_var(){
		//setStyle_lhcb();

		TString name="Lb_TAU";
		name="Lb_pim_ETA";
		name="Lb_pim_PT";
		name="Lb_ENDVERTEX_Z-Lc_ENDVERTEX_Z";
		//name="Lc_ENDVERTEX_Z";
		//name="DTFonlyPV_Lb_chi2"; 
		//name="nSPDHits";
		//name="nTracks";

		// *** Tree read ***
		TChain *tc1 = new TChain("DecayTree");
		tc1->Add("/publicfs2/ucas/user/wanghj/LHCb/run2/data/fit3/lfn_4748_Lb2Lcpi_Lc2ppipi_DATA16_loosecut_noLccut_tn_sWeight.root");
		TChain *tc2 = new TChain("DecayTree");
		tc2->Add("GBRforBDT_MC16.root");

		double xbin=50; 
		double xmin=0;
		double xmax=0.01;

		if(name=="Lb_TAU")            {xmin=0.00;xmax=0.01;}
		if(name=="Lb_pim_ETA")        {xmin=1.50;xmax=5.50;}
		if(name=="Lb_pim_PT")         {xmin=0.00;xmax=15000;}
		if(name=="Lb_ENDVERTEX_Z-Lc_ENDVERTEX_Z")    {xbin=150;xmin=-15.00;xmax=15.00;}
		if(name=="Lc_ENDVERTEX_Z")    {xmin=-150.00;xmax=150.00;}
		if(name=="DTFonlyPV_Lb_chi2") {xmin=0.00;xmax=40.00;}
		if(name=="nSPDHits")          {xmin=10.00;xmax=900.00;}
		if(name=="nTracks")           {xmin=10.00;xmax=600.00;}

		TH1F *p1   = new TH1F("p1","",xbin,xmin,xmax); p1->Sumw2();
		TH1F *p2   = new TH1F("p2","",xbin,xmin,xmax); p2->Sumw2();
		TH1F *p3   = new TH1F("p3","",xbin,xmin,xmax); p3->Sumw2();

		tc1->Project("p1", name,"sig_sw"); 
		tc2->Project("p2", name,"GBRforBDT");
		tc2->Project("p3", name,"1");
		p1->Scale(1.0/(double)p1->Integral());
		p2->Scale(1.0/(double)p2->Integral());
		p3->Scale(1.0/(double)p3->Integral());

		TH1F *p4   = new TH1F("p4","",xbin,xmin,xmax);
		p4->Sumw2();
		p4->Divide(p1,p2,1.0/(double)p1->Integral(),1.0/(double)p2->Integral());

		TCanvas *c1 = new TCanvas("c1","mDs",800,600);
		//c1->SetLogy();
		c1 -> Divide(1,2);
		c1->SetLeftMargin(0.01);
		c1->SetRightMargin(0.01);
		c1->SetTopMargin(0.01);
		c1->SetBottomMargin(0.0);

		c1 -> cd(1) -> SetPad(0.0, 0.0, 1.0, 0.25);
		gPad->SetTopMargin(0.00);
		gPad->SetBottomMargin(0.60);
		gPad->SetLeftMargin(0.12);

		TF1 *f_one = new TF1("f_one", "1", xmin, xmax);
		f_one->SetLineStyle(2);
		f_one->SetLineWidth(3);
		f_one->SetLineColor(kAzure+4);
		f_one->Draw();

		f_one->SetMaximum(3.5);
		f_one->SetMinimum(-1.5);
		f_one->GetYaxis()->SetNdivisions(505);
		f_one->GetYaxis()->SetLabelSize(0.08);    
		f_one->GetYaxis()->CenterTitle(true);
		f_one->GetYaxis()->SetTitle("#frac{sDATA}{wMC}");
		f_one->GetYaxis()->SetTitleSize(0.10);
		f_one->GetYaxis()->SetTitleOffset(0.5);
		f_one->GetXaxis()->SetNdivisions(505);
		f_one->GetXaxis()->SetLabelSize(0.2);
		//p4->GetXaxis()->CenterTitle(true);
		f_one->GetXaxis()->SetTitleSize(0.15);
		f_one->GetXaxis()->SetTitleOffset(1.20);
		f_one->GetXaxis()->SetTitleFont(22);
		f_one->GetYaxis()->SetTitleFont(22);
		f_one->GetXaxis()->SetLabelFont(22);
		f_one->GetYaxis()->SetLabelFont(22);
		f_one->GetXaxis()->SetTitle(name);
		p4->SetLineColor(1);
		p4->SetLineWidth(3);
		p4->Draw("same hist lep");

		c1 -> cd(2) -> SetPad(0.0, 0.25, 1.0, 1.0);
		gPad->SetTopMargin(0.02);
		gPad->SetBottomMargin(0.00);
		gPad->SetLeftMargin(0.12);

		//c1 -> cd(2) -> SetLogy();
		p1->GetYaxis()->SetMaxDigits(5);
		p1->SetMinimum(0.001);
		p1->SetMaximum(p1->GetMaximum()*1.25);

		p1->GetYaxis()->SetNdivisions(505);
		p1->GetYaxis()->SetLabelSize(0.07);    
		p1->GetYaxis()->CenterTitle(true);
		p1->GetYaxis()->SetTitle("Normalization");
		p1->GetYaxis()->SetTitleSize(0.06);
		p1->GetYaxis()->SetTitleOffset(0.9);
		p1->GetXaxis()->SetNdivisions(505);
		p1->GetXaxis()->SetLabelSize(0.2);
		p1->GetXaxis()->CenterTitle(true);
		p1->GetXaxis()->SetTitleSize(1.2);
		p1->GetXaxis()->SetTitleOffset(1.00);
		p1->GetXaxis()->SetTitleFont(22);
		p1->GetYaxis()->SetTitleFont(22);
		p1->GetXaxis()->SetLabelFont(22);
		p1->GetYaxis()->SetLabelFont(22);

		TLegend *legend = new TLegend(0.6115288,0.650463,0.8571429,0.9421296,NULL,"brNDC");
		legend->AddEntry(p1, "sweighted DATA16","lep");
		legend->AddEntry(p2, "GB Reweighted MC16","l");
		legend->AddEntry(p3, "Original MC16","l");
		legend->SetFillColor(0);
		legend->SetBorderSize(0);
		legend->SetLineColor(1);
		legend->SetLineStyle(1);
		legend->SetLineWidth(1);
		legend->SetTextFont(22);
		legend->SetFillStyle(1001);

		p1->SetLineColor(2);
		p2->SetLineColor(1);
		p3->SetLineColor(8);
		p1->SetLineWidth(3);
		p2->SetLineWidth(3);
		p3->SetLineWidth(3);

		p1->Draw("lep");         
		p2->Draw("same hist"); 
		p3->Draw("same hist");
		gStyle->SetOptStat(0);

		legend->Draw("same");

		c1->SaveAs(name+".pdf");
}

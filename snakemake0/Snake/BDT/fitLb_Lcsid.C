#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "RooFitResult.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooHistPdf.h"
#include "RooAddPdf.h"
#include "RooStats/SPlot.h"
#include "TChain.h"
#include "RooHypatia2.h"
#include "RooJohnson.h"
#include "RooLinearVar.h"
#include "RooChebychev.h"
#include "RooFormulaVar.h"
#include "TAxis.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TPaveText.h"
#include "RooFFTConvPdf.h"
#include <iomanip>
#include "RooMinimizer.h"
#include "/lzufs/home/wanghj/lhcbStyle.h"
#include "/publicfs2/ucas/user/niuqile/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake0/Snake/cut.h"

using namespace std;
using namespace RooFit;

void fitLb_Lcsid(string infile, TString txtfile, TString outfile_plot_Lbsid){
		//fit Lb mass spectrum to obtain the scale from sideband to signal
		//in order to fix the yield of pure 4-body decay from Lb
		//string infile="/publicfs2/ucas/user/wanghj/LHCb/run2/snakemake/Output/BDT/BDTApp/DATA_Lb2Lcpi_2016_MU_BDTApp.root";
    //TString txtfile="model.txt";
		//TString outfile_plot_Lbsid="test_Lcsid.png";

		lhcbStyle();

		double min=5520,max=5720;
		int bin = 22;
		TChain *tc = new TChain("DecayTree");
		tc->Add(infile.c_str());
		TTree *t = tc->CopyTree(cut_Lc_sideband + cut_BDTG, "", 1000000);
		cout << t -> GetEntries();

		RooRealVar *DTFonlyPV_Lb_M = new RooRealVar("DTFonlyPV_Lb_M", "M(#it{p#pi^{+}#pi^{-}}) (MeV/#it{c}^{2})", min, max);

		RooRealVar *Lb4_mean = new RooRealVar("Lb4_mean", "mean", 5619, 5600, 5640);
		RooRealVar *Lb4_sigma  = new RooRealVar("Lb4_sigma", "sigma", 10, 3, 25);
		RooGaussian *gaussian=new RooGaussian("gaussian","gaussian",*DTFonlyPV_Lb_M,*Lb4_mean,*Lb4_sigma);

		//Poly. for comb bkg
		RooRealVar *Lbbkg_c0 = new RooRealVar("Lbbkg_c0", "Lbbkg_c0", -2.8445e-01, -1.0, 0.0);
		RooChebychev *Lbbkg_chey = new RooChebychev("Lbbkg_chey", "Lbbkg_chey", *DTFonlyPV_Lb_M, RooArgList(*Lbbkg_c0));

		RooRealVar *n_signal =   new RooRealVar("n_signal", "n_signal", 1.1652e+02, 10, 200);
		RooRealVar *n_chey =   new RooRealVar("n_chey", "n_background", 1.0745e+03, 10, 1500);

		RooAddPdf *model = new RooAddPdf("model", "model", 
						RooArgList(*gaussian,*Lbbkg_chey),
						RooArgList(*n_signal,*n_chey));

		RooDataSet *dataset = new RooDataSet("dataset", "dataset", t, *DTFonlyPV_Lb_M);

		//Fit
		auto r = model-> fitTo(*dataset, Save(), Extended());
		r -> Print("v");

		DTFonlyPV_Lb_M->setRange("signal",5550, 5700);
		RooAbsReal* sig_sigregion = gaussian->createIntegral(*DTFonlyPV_Lb_M,NormSet(*DTFonlyPV_Lb_M),Range("signal")) ;

		//Plot
		auto frame = DTFonlyPV_Lb_M -> frame(min, max, bin);
		frame->GetYaxis()->SetTitle(Form("Events / (%0.1f MeV/#it{c}^{2})",(max-min)/bin));
		frame->GetXaxis()->SetTitleFont(22);
		frame->GetYaxis()->SetTitleFont(22);
		frame->GetXaxis()->SetLabelFont(22);
		frame->GetYaxis()->SetLabelFont(22);
		frame->GetXaxis()->SetTitleOffset(1.00);

		dataset -> plotOn(frame, Name("mc"),DataError(RooAbsData::Poisson));
		model -> plotOn(frame, Name("sum"), LineStyle(1), LineColor(kSpring-6));
		RooHist *pull = frame -> pullHist();
		model -> plotOn(frame, Components("gaussian"), Name("gaussian"),    LineStyle(2), LineColor(kMagenta-2));
		model -> plotOn(frame, Components("Lbbkg_chey"), Name("Lbbkg_chey"), LineStyle(2), LineColor(kYellow-2));

		//Pull
		auto frame_pull = DTFonlyPV_Lb_M -> frame(min, max, bin);

		RooConstVar F_zero("F_zero", "F_zero", 0);
		RooConstVar F_up("F_up", "F_up", 3);
		RooConstVar F_down("F_down", "F_down", -3);

		F_zero.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kBlue));
		F_up.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));
		F_down.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));

		frame_pull->addPlotable(pull, "P");

		TCanvas *cs = new TCanvas("cs", "cs", 800, 600);
		cs -> Divide(1,2);
		cs->SetLeftMargin(0.01);
		cs->SetRightMargin(0.01);
		cs->SetTopMargin(0.01);
		cs->SetBottomMargin(0.0);

		cs -> cd(1) -> SetPad(0.0, 0.0, 1.0, 0.25);
		gPad->SetTopMargin(0.00);
		gPad->SetBottomMargin(0.60);
		gPad->SetLeftMargin(0.12);
		//gPad->SetLogy();
		frame_pull->SetMaximum(7);
		frame_pull->SetMinimum(-7);
		frame_pull->GetYaxis()->SetNdivisions(104);
		frame_pull->GetYaxis()->SetLabelSize(0.15);    
		frame_pull->GetYaxis()->CenterTitle(true);
		frame_pull->GetYaxis()->SetTitle("Pull");
		frame_pull->GetYaxis()->SetTitleSize(0.20);
		frame_pull->GetYaxis()->SetTitleOffset(0.29);
		frame_pull->GetXaxis()->SetNdivisions(505);
		frame_pull->GetXaxis()->SetLabelSize(0.2);
		//frame_pull->GetXaxis()->CenterTitle(true);
		frame_pull->GetXaxis()->SetTitleSize(0.22);
		frame_pull->GetXaxis()->SetTitleOffset(0.85);
		frame_pull->GetXaxis()->SetTitleFont(22);
		frame_pull->GetYaxis()->SetTitleFont(22);
		frame_pull->GetXaxis()->SetLabelFont(22);
		frame_pull->GetYaxis()->SetLabelFont(22);
		frame_pull->Draw();

		TString a("Events/");char b[20];sprintf(b, "(%1.1f",(max-min)/bin); TString c(" MeV/c^{2})"); TString ytitle = a + b + c;

		cs -> cd(2) -> SetPad(0.0, 0.25, 1.0, 1.0);
		gPad->SetTopMargin(0.02);
		gPad->SetBottomMargin(0.00);
		gPad->SetLeftMargin(0.12);
		frame->GetYaxis()->SetTitle(ytitle);
		frame->GetXaxis()->SetLabelOffset(0.02);
		frame->GetXaxis()->SetLabelSize(0.05);
		frame->GetXaxis()->SetTitleSize(0.06);
		frame->GetXaxis()->SetTitleOffset(1.1);
		frame->GetYaxis()->CenterTitle(true);
		frame->GetYaxis()->SetLabelOffset(0.015);
		frame->GetYaxis()->SetLabelSize(0.070);
		frame->GetYaxis()->SetTitleSize(0.070);
		frame->GetYaxis()->SetTitleOffset(0.85);
		frame->GetYaxis()->SetMaxDigits(5);
		frame->SetMinimum(0.0001);
		frame->SetMaximum(160);
		frame->Draw();

		TPaveText *pt = new TPaveText(0.6365915,0.1018519,0.8696742,0.2430556,"BRNDC");
		pt->SetBorderSize(0);
		pt->SetFillColor(10);
		pt->SetFillStyle(1001);
		pt->SetTextAlign(12);
		pt->SetTextSize(0.06);
		pt->SetTextFont(22);
		TText *text;
		text = pt->AddText("#it{#Lambda_{b}^{0}#rightarrow#Lambda_{c}^{+}K^{-}, #Lambda_{c}^{+}#rightarrowp#pi^{+}#pi^{-}}");
		//pt->Draw("same");
		TLegend *leg = new TLegend(0.1654135,0.599537,0.3596491,0.9305556,NULL,"brNDC");
		leg->AddEntry(frame -> findObject("mc"), "MC", "lep");
		leg->AddEntry(frame -> findObject("sum"), "Fit result", "l");
		leg->AddEntry(frame -> findObject("gaussian"), "Signal", "l");
		leg->AddEntry(frame -> findObject("Lbbkg_chey"), "Background", "l");
		leg->SetFillStyle(1001);
		leg->SetTextSize(0.06);
		leg->SetBorderSize(0);
		leg->SetTextFont(22);
		leg->Draw("same");

        auto params = gaussian -> getParameters(*DTFonlyPV_Lb_M);
        Lb4_mean   -> setConstant();
        Lb4_sigma  -> setConstant();
        params->writeToFile(txtfile);

		TPaveText *pt1 = new TPaveText(0.5202256,0.6018519,0.8272431,0.9328704,"BRNDC");
		pt1->SetBorderSize(0);
		pt1->SetFillColor(10);
		pt1->SetFillStyle(1001);
		pt1->SetTextAlign(12);
		pt1->SetTextSize(0.06);
		pt1->SetTextFont(22);
		TString str1=Form("mean=%1.2f#pm%1.2f",Lb4_mean->getVal(),Lb4_mean->getError());
		TString str2=Form("sigma=%1.2f#pm%1.2f",Lb4_sigma->getVal(),Lb4_sigma->getError());
		TString str3=Form("N_{sig}=%1.2f#pm%1.2f",n_signal->getVal(),n_signal->getError());
		//TString str4=Form("N_{bkg}=%1.2f#pm%1.2f",n_chey->getVal(),n_chey->getError());
		//TString str5=Form("Chebychev para=%1.2f#pm%1.2f",Lbbkg_c0->getVal(),Lbbkg_c0->getError());

cout<<"RainbowNsig4="<<fixed<<setprecision(4)<<n_signal->getVal()<<endl;
cout<<"RainbowNsig4err="<<fixed<<setprecision(4)<<n_signal->getError()<<endl;

		TText *text1;
		text1 = pt1->AddText(str1);
		text1 = pt1->AddText(str2);
		text1 = pt1->AddText(str3);
		//text1 = pt1->AddText(str4);
		//text1 = pt1->AddText(str5);
		//text1 = pt1->AddText(str6);
		pt1->Draw("same");

		cs->Update();
		cs->Print(outfile_plot_Lbsid);
}

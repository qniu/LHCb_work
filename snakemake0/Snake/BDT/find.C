void find(){
		if (!gROOT->IsBatch()) {
				TMVA::TMVAGui("test.root");
				gSystem->ProcessEvents();
				gSystem->Sleep(2000); // Adjust the sleep time if necessary

				TIter next(gROOT->GetListOfCanvases());
				TCanvas *canvas;
				while ((canvas = (TCanvas*)next())) {
						TString canvasName = canvas->GetName();
						TString fileName = canvasName + ".png";
						//canvas->SaveAs(fileName);
						std::cout << "Saved canvas: " << canvasName << " as " << fileName << std::endl;
				}
		}
}

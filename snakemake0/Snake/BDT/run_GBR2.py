import pandas as pd
import numpy as np
from hep_ml.reweight import GBReweighter
import matplotlib.pyplot as plt
from scipy.stats import ks_2samp,kstest
import sys
import re

def weighted_ks_test(data, data_weights, mc, mc_weights):
    data_cdf = np.cumsum(data_weights[np.argsort(data)]) / np.sum(data_weights)
    mc_cdf = np.cumsum(mc_weights[np.argsort(mc)]) / np.sum(mc_weights)

    data_sorted = np.sort(data)
    mc_sorted = np.sort(mc)
    
    all_values = np.sort(np.concatenate([data_sorted, mc_sorted]))
    
    data_cdf_interp = np.interp(all_values, data_sorted, data_cdf, left=0, right=1)
    mc_cdf_interp = np.interp(all_values, mc_sorted, mc_cdf, left=0, right=1)
    
    ks_stat = np.max(np.abs(data_cdf_interp - mc_cdf_interp))
   
    n1 = len(data)
    n2 = len(mc)
    en = np.sqrt(n1 * n2 / (n1 + n2))
    p_value = kstest([ks_stat], 'uniform', alternative='two-sided').pvalue
 
    return ks_stat,p_value

def myGBR(input_data_file, input_mc_file, output_file):

    pattern = r'(\d{4})_([A-Z]+)'
    match = re.search(pattern, input_data_file)
    year = match.group(1)
    polarity = match.group(2)
    polarities = {"MU": "MagUp", "MD": "MagDown"}
    polarityyear = polarities[polarity] + "_" + year

    data_df = pd.read_csv(input_data_file, delim_whitespace=True, header=None)
    mc_df = pd.read_csv(input_mc_file, delim_whitespace=True, header=None)

    data_df.columns = ['sig_sw'] + [f'var_{i}' for i in range(1, len(data_df.columns))]
    mc_df.columns = [f'var_{i}' for i in range(1, len(mc_df.columns) + 1)]

    variables = [f'var_{i}' for i in range(1, len(data_df.columns))]
    source_vars = mc_df[variables].values
    target_vars = data_df[variables].values

    data_weights = data_df['sig_sw'].values
    mc_weights = np.ones(source_vars.shape[0])

    reweighter = GBReweighter(n_estimators=200, learning_rate=0.1, max_depth=5, min_samples_leaf=10, gb_args={'subsample': 0.7})
    reweighter.fit(source_vars, target_vars, mc_weights, data_weights)
    new_weights = reweighter.predict_weights(source_vars)

    mc_df['GBRweight'] = new_weights
    mc_df[['GBRweight']].to_csv(output_file, header=False, index=False, sep=' ')

    min_values = [0.000, 1.4, 0.000, -20, 0]
    max_values = [0.015, 5.4, 15000,  10, 7]
    #x_labels = ["Lb_TAU","Lb_pim_ETA","Lb_pim_PT","Lb_VectorZ","Lb_DTFonlyPV_chi2onDOF"]
    x_labels = ["TAU(Lb)","ETA(Lb_pim)","PT(Lb_pim)","VectorZ(Lb)","chi2/nDOF(onlyPV)"]

    for i, var in enumerate(variables):
        min_range = min_values[i]
        max_range = max_values[i]

        data_weights_normalized = data_df['sig_sw'] / data_df['sig_sw'].sum()
        mc_weights_normalized = mc_df['GBRweight'] / mc_df['GBRweight'].sum()

        plt.figure()
        plt.hist(data_df[var], bins=50, weights=data_weights_normalized, alpha=0.5, label='sFit Data', density=True, range=(min_range, max_range), color='blue')
        plt.hist(mc_df[var], bins=50, weights=mc_weights_normalized, alpha=0.5, label='Weighted MC', density=True, range=(min_range, max_range), color='orange')
        plt.hist(mc_df[var], bins=50, alpha=0.5, label='Original MC', density=True, range=(min_range, max_range), color='green')

        plt.xlabel(x_labels[i], fontsize=14) 
        plt.ylabel('Density', fontsize=14)   
        plt.legend(fontsize=12)              
        #plt.title(f'Distribution of {var}', fontsize=16)

        plt.subplots_adjust(left=0.15)

        plt.savefig(f'/lzufs/user/wanghj/LHCb/Lc2ppipi/snakemake/Log/GBRforBDT/{var}_comparison_{polarityyear}.png')
        plt.close()

        limited_data_var = data_df[var]
        limited_data_weights = data_weights_normalized
        
        limited_mc_var = mc_df[var]
        limited_mc_weights = mc_weights_normalized

        #ks_stat, p_value = weighted_ks_test(limited_data_var, limited_data_weights, limited_mc_var, limited_mc_weights)
        #print(f'Weighted KS test for {var}: KS statistic = {ks_stat}, p-value = {p_value}')

if __name__ == '__main__':
    myGBR(sys.argv[1], sys.argv[2], sys.argv[3])

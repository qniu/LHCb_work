#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "iostream"
#include <iomanip>
#include "TString.h"
#include "TCut.h"
#include "TChain.h"
#include "/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake0/Snake/cut.h"

using namespace std;
typedef std::vector<int> Vint;
typedef std::vector<double> Vdouble;

void pid(TString RootChainIN, TString RootChainOUT){
		TChain* chain  = new TChain("DecayTree");
		chain->Add(RootChainIN);
		TFile* file_chain = new TFile(RootChainOUT,"recreate");
    cout<<"input : "<<RootChainIN<<endl;
    cout<<"outfile : "<<RootChainOUT<<endl;
    cout<<"cut : "<<cut_PID.GetTitle()<<endl<<endl;
		TTree *recchain = chain->CopyTree(cut_PID);
		recchain->Write();
		file_chain->Close();
}

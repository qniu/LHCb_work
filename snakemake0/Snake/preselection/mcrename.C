#include "TCut.h"
#include <string>
#include <ROOT/RDataFrame.hxx>
using namespace std;

int mcrename(string infile, string outfile){
		string treename = "DecayTree";
		ROOT::RDataFrame myDF(treename, infile);
		if(infile.find("") != std::string::npos){
				auto newDF = myDF
						.Define(     "Pp_ProbNNk_old",  "Pp_ProbNNk") .Redefine(     "Pp_ProbNNk",     "Pp_ProbNNk_pidcorr_default")
						.Define(    "Pp_ProbNNpi_old",  "Pp_ProbNNpi") .Redefine(    "Pp_ProbNNpi",    "Pp_ProbNNpi_pidcorr_default")
						.Define(     "Pp_ProbNNp_old",  "Pp_ProbNNp") .Redefine(     "Pp_ProbNNp",     "Pp_ProbNNp_pidcorr_default")
						.Define(    "Kp_ProbNNk_old",   "Kp_ProbNNk") .Redefine(   "Kp_ProbNNk",    "Kp_ProbNNk_pidcorr_default")
						.Define(   "Kp_ProbNNpi_old",   "Kp_ProbNNpi") .Redefine(   "Kp_ProbNNpi",   "Kp_ProbNNpi_pidcorr_default")
						.Define(    "Kp_ProbNNp_old",   "Kp_ProbNNp") .Redefine(    "Kp_ProbNNp",    "Kp_ProbNNp_pidcorr_default")
						.Define(    "Km_ProbNNk_old",   "Km_ProbNNk") .Redefine(    "Km_ProbNNk",    "Km_ProbNNk_pidcorr_default")
						.Define(   "Km_ProbNNpi_old",   "Km_ProbNNpi") .Redefine(   "Km_ProbNNpi",   "Km_ProbNNpi_pidcorr_default")
						.Define(    "Km_ProbNNp_old",    "Km_ProbNNp") .Redefine(    "Km_ProbNNp",    "Km_ProbNNp_pidcorr_default")
						.Define( "Pim_ProbNNk_old", "Pim_ProbNNk") .Redefine( "Pim_ProbNNk", "Pim_ProbNNk_pidcorr_default")
						.Define("Pim_ProbNNpi_old","Pim_ProbNNpi") .Redefine("Pim_ProbNNpi","Pim_ProbNNpi_pidcorr_default")
						.Define( "Pim_ProbNNp_old", "Pim_ProbNNp") .Redefine( "Pim_ProbNNp", "Pim_ProbNNp_pidcorr_default")
						.Snapshot(treename, outfile);
		}
		return 0;
}

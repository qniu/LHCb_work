###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

import os
import sys
import re

def tracks_Lcpi():
    track_Lcpi = {
        'Lb_pim': {
            "ProbNNk": "pi_MC15TuneV1_ProbNNK_Brunel",
            "ProbNNpi":"pi_MC15TuneV1_ProbNNpi_Brunel_Mod2",
            "ProbNNp": "pi_MC15TuneV1_ProbNNp_Brunel",
        },
    }
    return track_Lcpi

def tracks_LcK():
    track_LcK = {
        'Lb_pim': {
            "ProbNNk": "K_MC15TuneV1_ProbNNK_Brunel",
            "ProbNNpi":"K_MC15TuneV1_ProbNNpi_Brunel",
            "ProbNNp": "K_MC15TuneV1_ProbNNp_Brunel",
        },
    }
    return track_LcK

def myPIDcorr(infile, outfile, trackscale):

    #####################
    #  START OF CONFIG  #
    #####################

    # look for year and polarity in file names
    pattern = r'(\d{4})_([A-Z]+)'
    match = re.search(pattern, outfile)
    year = match.group(1)
    polarity = match.group(2)
    polarities = {"MU": "MagUp", "MD": "MagDown"}
    polarityyear = polarities[polarity] + "_" + year

    # List of input ROOT files with MC ntuples. Format:
    #   (inputfile, outputfile, dataset)
    files = [(infile, outfile, trackscale, polarityyear)]

    tracks = {
       'pp': {
           "ProbNNk": "p_LbLcPi_MC15TuneV1_ProbNNK_Brunel",
           "ProbNNpi":"p_LbLcPi_MC15TuneV1_ProbNNpi_Brunel",
           "ProbNNp": "p_LbLcPi_MC15TuneV1_ProbNNp_Brunel",
       },
       'pip': {
           "ProbNNk": "pi_MC15TuneV1_ProbNNK_Brunel",
           "ProbNNpi":"pi_MC15TuneV1_ProbNNpi_Brunel_Mod2",
           "ProbNNp": "pi_MC15TuneV1_ProbNNp_Brunel",
       },
       'pim': {
           "ProbNNk": "pi_MC15TuneV1_ProbNNK_Brunel",
           "ProbNNpi":"pi_MC15TuneV1_ProbNNpi_Brunel_Mod2",
           "ProbNNp": "pi_MC15TuneV1_ProbNNp_Brunel",
       },
    }
    
    input_tree = "DecayTree"
    if "Lcpi" in infile:
        # Name of the input tree
        tracks.update(tracks_Lcpi())
    elif "LcK" in infile:
        # Name of the input tree
        tracks.update(tracks_LcK())
        
    print(tracks)

    # Simulation version
    simversion = "run2"

    # Postfixes of the Pt, Eta and Ntracks variables in branch names
    # e.g. if the ntuple contains "pion_PT", it should be just "PT"
    ptvar = "PT"
    etavar = "ETA"
    pvar = None
    ## Could use P variable instead of eta
    # etavar = None
    # pvar   = "p"
    ntrvar = "nTracks"  # This should correspond to the number of "Best tracks", not "Long tracks"!

    # Remove TDirectory in output tree
    output_tree = input_tree
    treename = input_tree

    #############################
    #  Correction with PIDCorr  #
    #############################
    # Loop over the file
    for input_file, output_file, ntrscale, dataset in files:
        # Initialize correction command in bash
        commands = ""
        tmpinfile = input_file

        # Prepare temporary files
        tmpoutfile = output_file.replace(".root", "_temp1.root")
        # Loop on the tracks
        for track, subst in tracks.items():
            # Loop on the PID variables 
            for var, config in subst.items():
                # Use PIDCorr from LHCb software
                # command = "lb-run -c best Urania/v10r1 python /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v10r1/InstallArea/x86_64_v2-centos7-gcc11-opt/python/PIDPerfScripts/PIDCorr.py"
                command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDCorr.py"
                command += " -m %s_%s" % (track, ptvar)
                if etavar:
                    command += " -e %s_%s" % (track, etavar)
                elif pvar:
                    command += " -q %s_%s" % (track, pvar)
                else:
                    print('Specify either ETA or P branch name per track')
                    sys.exit(1)
                command += " -n %s" % ntrvar
                command += " -t %s" % treename
                command += " -p %s_%s_corr" % (track, var)
                command += " -s %s_%s" % (track, var)
                command += " -c %s" % config
                command += " -d %s" % dataset
                command += " -i %s" % tmpinfile
                command += " -o %s" % tmpoutfile
                command += " -S %s" % simversion
                command += " -f %s" % ntrscale

                # Prepare for correction of the next PID variable
                treename = output_tree
                tmpinfile = tmpoutfile
                if 'temp1' in tmpoutfile:
                    tmpoutfile = tmpoutfile.replace('temp1', 'temp2')
                else:
                    tmpoutfile = tmpoutfile.replace('temp2', 'temp1')

                commands += (command + "\n")

        if "3pie" in input_file:
            command = "python $PIDPERFSCRIPTSROOT/scripts/python/PIDGenUser/PIDGen.py"
            command += " -m %s_%s" % ("taulep", ptvar)
            if etavar:
                command += " -e %s_%s" % ("taulep", etavar)
            elif pvar:
                command += " -q %s_%s" % ("taulep", pvar)
            else:
                print('Specify either ETA or P branch name per track')
                sys.exit(1)
            command += " -n %s" % ntrvar
            command += " -t %s" % treename
            command += " -p %s_%s_corr" % ("taulep", "ProbNNe")
            command += " -c %s" % "e_MC15TuneV1_ProbNNe_Stripping"
            command += " -d %s" % polarityyear
            command += " -i %s" % tmpinfile
            command += " -o %s" % tmpoutfile
            command += " -a"

            treename = output_tree
            tmpinfile = tmpoutfile
            if 'temp1' in tmpoutfile:
                tmpoutfile = tmpoutfile.replace('temp1', 'temp2')
            else:
                tmpoutfile = tmpoutfile.replace('temp2', 'temp1')
            
            commands += (command + "\n")

        #########################################
        #  Save file and clean temporary files  #
        #########################################
        commands += " cp %s %s\n" % (tmpinfile, output_file)
        rmfile = output_file.replace(".root", "_temp*.root")
        commands += " rm %s\n" %rmfile

        ###################################
        #  Output commands into terminal  #
        ###################################
        print(commands)
        os.system(commands)

if __name__ == '__main__':
    myPIDcorr(sys.argv[1], sys.argv[2], sys.argv[3])

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "iostream"
#include <iomanip>
#include "TString.h"
#include "TCut.h"
#include "TChain.h"
#include <set>
#include <fstream>
#include "/lzufs/home/wanghj/RootUtil.h"
#include "/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Snake/cut.h"

using namespace std;

//void rmtrack(TString infile, TString outfile){
//void Clone_Multi_check(TString infile, TString outfile){
void Clone_Multi_check(){
	TString infile="/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Output/merged/DATA_Lb2Lcpi_2016_merged.root";
	TString outfile="test_clone_mul.root";
	TChain *partree0 = new TChain("DecayTree");
	partree0->Add(infile);

	TCut cut_PID_final="1";
	if (infile.Index("2016") != kNPOS) {
		cut_PID_final = "1";
	}
	if (infile.Index("2017") != kNPOS) {
		cut_PID_final = "1";
	}
	if (infile.Index("2018") != kNPOS) {
		cut_PID_final = "1";
	}

	TTree *partree=partree0->CopyTree(cut_PID_final);

	TFile * skimfile = new TFile (outfile,"recreate");
	//	TTree* t = partree->CloneTree(0);

	Double_t Pim_PE;
	Double_t Pim_PX;
	Double_t Pim_PY;
	Double_t Pim_PZ;
	Double_t Pp_PE;
	Double_t Pp_PX;
	Double_t Pp_PY;
	Double_t Pp_PZ;
	Double_t Kp_PE;
	Double_t Kp_PX;
	Double_t Kp_PY;
	Double_t Kp_PZ;
	Double_t Km_PE;
	Double_t Km_PX;
	Double_t Km_PY;
	Double_t Km_PZ;

	UInt_t runNumber;
	ULong64_t eventNumber;

	partree->SetBranchAddress("Pim_PE",   &Pim_PE);
	partree->SetBranchAddress("Pim_PX",   &Pim_PX);
	partree->SetBranchAddress("Pim_PY",   &Pim_PY);
	partree->SetBranchAddress("Pim_PZ",   &Pim_PZ);
	partree->SetBranchAddress("Pp_PE",    &Pp_PE);
	partree->SetBranchAddress("Pp_PX",    &Pp_PX);
	partree->SetBranchAddress("Pp_PY",    &Pp_PY);
	partree->SetBranchAddress("Pp_PZ",    &Pp_PZ);
	partree->SetBranchAddress("Kp_PE",    &Kp_PE);
	partree->SetBranchAddress("Kp_PX",    &Kp_PX);
	partree->SetBranchAddress("Kp_PY",    &Kp_PY);
	partree->SetBranchAddress("Kp_PZ",    &Kp_PZ);
	partree->SetBranchAddress("Km_PE",    &Km_PE);
	partree->SetBranchAddress("Km_PX",    &Km_PX);
	partree->SetBranchAddress("Km_PY",    &Km_PY);
	partree->SetBranchAddress("Km_PZ",    &Km_PZ);

	partree->SetBranchAddress("runNumber",   &runNumber);
	partree->SetBranchAddress("eventNumber", &eventNumber);

	int Nmulti;
	double DoM_Pp_Kp;
	double DoM_Pim_Km;

	//		t->Branch("Nmulti", &Nmulti, Nmulti);
	//		t->Branch("DoM_pp_pip", &DoM_pp_pip, DoM_pp_pip);
	//		t->Branch("DoM_Lb_pim_pim", &DoM_Lb_pim_pim, DoM_Lb_pim_pim);

	std::map<std::pair<int, int>, int> eventCount;
	std::set<int> uniqueIndices;

	for (int i = 0; i < partree->GetEntries(); i++) {
		partree->GetEntry(i);
		std::pair<int, int> key = std::make_pair(runNumber, eventNumber);
		if (eventCount[key] == 0) {
			uniqueIndices.insert(i);
		}
		eventCount[key]++;
	}

	double flag=sqrt((2.0201-2.02*cos(0.0005))/(2.0201+2.02*cos(0.0005)));
	int Nclo=0;
	int Nmul=0;

	for (int i : uniqueIndices) {
		partree->GetEntry(i);
		TLorentzVector Pim0(Pim_PX,Pim_PY,Pim_PZ,Pim_PE);
		TLorentzVector Pp0(Pp_PX,Pp_PY,Pp_PZ,Pp_PE);
		TLorentzVector Kp0(Kp_PX,Kp_PY,Kp_PZ,Kp_PE);
		TLorentzVector Km0(Km_PX,Km_PY,Km_PZ,Km_PE);

		DoM_Pp_Kp=(Pp0-Kp0).Rho()/(Pp0+Kp0).Rho();
		DoM_Pim_Km=(Pim0-Km0).Rho()/(Pim0+Km0).Rho();

		if((DoM_Pp_Kp<flag)||(DoM_Pim_Km<flag)) Nclo++;//{Nclo++; continue;}

	std::pair<int, int> key = std::make_pair(runNumber, eventNumber);
	Nmulti = eventCount[key];

	if(Nmulti>1) {Nmul++;
cout<<" runNumber :"<<runNumber<<"   eventNumber:"<<eventNumber<<endl;
		//	t->Fill();
	}
	}
	double NN=(double)partree->GetEntries();

	cout<<"Total : "<<NN<<endl;
	cout<<"NClone : "<<Nclo<<" ("<<(double)Nclo/NN*100<<"\%)"<<endl;
	cout<<"NMulti : "<<Nmul<<" ("<<(double)Nmul/NN*100<<"\%)"<<endl;

	//		t->Write();
	skimfile->Close();
}

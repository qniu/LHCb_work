/// \file
/// \ingroup tutorial_tmva
/// \notebook -nodraw
/// This macro provides a simple example on how to use the trained classifiers
/// within an analysis module
/// - Project   : TMVA - a Root-integrated toolkit for multivariate data analysis
/// - Package   : TMVA
/// - Exectuable: TMVAClassificationApplication
///
/// \macro_output
/// \macro_code
/// \author Andreas Hoecker

#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>

#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TChain.h"

#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include "/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Snake/cut.h"


using namespace TMVA;

void TMVAApplication(TString inputfile, TString outputfile, TString model, TString tempfile){

//		TString inputfile = "/publicfs2/ucas/user/wanghj/LHCb/run2/snakemake/Output/addvariables/DATA_Lb2Lcpi_2018_MD_addvariables.root";
//		TString outputfile = "test.root";
//		TString model = "/publicfs2/ucas/user/wanghj/LHCb/run2/snakemake/Output/BDT/BDTtrain/model_2018_MD/dataset/weights/";

		//---------------------------------------------------------------
		// This loads the library
		TMVA::Tools::Instance();

		// Default MVA methods to be trained + tested
		std::map<std::string,int> Use;

		// Cut optimisation
		Use["Cuts"]            = 0;
		Use["CutsD"]           = 0;
		Use["CutsPCA"]         = 0;
		Use["CutsGA"]          = 0;
		Use["CutsSA"]          = 0;
		//
		// 1-dimensional likelihood ("naive Bayes estimator")
		Use["Likelihood"]      = 0;
		Use["LikelihoodD"]     = 0; // the "D" extension indicates decorrelated input variables (see option strings)
		Use["LikelihoodPCA"]   = 0; // the "PCA" extension indicates PCA-transformed input variables (see option strings)
		Use["LikelihoodKDE"]   = 0;
		Use["LikelihoodMIX"]   = 0;
		//
		// Mutidimensional likelihood and Nearest-Neighbour methods
		Use["PDERS"]           = 0;
		Use["PDERSD"]          = 0;
		Use["PDERSPCA"]        = 0;
		Use["PDEFoam"]         = 0;
		Use["PDEFoamBoost"]    = 0; // uses generalised MVA method boosting
		Use["KNN"]             = 0; // k-nearest neighbour method
		//
		// Linear Discriminant Analysis
		Use["LD"]              = 0; // Linear Discriminant identical to Fisher
		Use["Fisher"]          = 0;
		Use["FisherG"]         = 0;
		Use["BoostedFisher"]   = 0; // uses generalised MVA method boosting
		Use["HMatrix"]         = 0;
		//
		// Function Discriminant analysis
		Use["FDA_GA"]          = 0; // minimisation of user-defined function using Genetics Algorithm
		Use["FDA_SA"]          = 0;
		Use["FDA_MC"]          = 0;
		Use["FDA_MT"]          = 0;
		Use["FDA_GAMT"]        = 0;
		Use["FDA_MCMT"]        = 0;
		//
		// Neural Networks (all are feed-forward Multilayer Perceptrons)
		Use["MLP"]             = 0; // Recommended ANN
		Use["MLPBFGS"]         = 0; // Recommended ANN with optional training method
		Use["MLPBNN"]          = 0; // Recommended ANN with BFGS training method and bayesian regulator
		Use["CFMlpANN"]        = 0; // Depreciated ANN from ALEPH
		Use["TMlpANN"]         = 0; // ROOT's own ANN
		Use["DNN_CPU"] = 0;         // CUDA-accelerated DNN training.
		Use["DNN_GPU"] = 0;         // Multi-core accelerated DNN.
		//
		// Support Vector Machine
		Use["SVM"]             = 0;
		//
		// Boosted Decision Trees
		Use["BDT"]             = 1; // uses Adaptive Boost
		Use["BDTG"]            = 0; // uses Gradient Boost
		Use["BDTB"]            = 0; // uses Bagging
		Use["BDTD"]            = 0; // decorrelation + Adaptive Boost
		Use["BDTF"]            = 0; // allow usage of fisher discriminant for node splitting
		//
		// Friedman's RuleFit method, ie, an optimised series of cuts ("rules")
		Use["RuleFit"]         = 0;
		// ---------------------------------------------------------------
		Use["Plugin"]          = 0;
		Use["Category"]        = 0;
		Use["SVM_Gauss"]       = 0;
		Use["SVM_Poly"]        = 0;
		Use["SVM_Lin"]         = 0;

		std::cout << std::endl;
		std::cout << "==> Start TMVAClassificationApplication" << std::endl;
		// --------------------------------------------------------------------------------------------------
		// Create the Reader object

		TMVA::Reader *reader = new TMVA::Reader( "!Color:!Silent" );

		// Create a set of variables and declare them to the reader
		// - the variable names MUST corresponds in name and type to those given in the weight file(s) used
		Float_t var1, var2, var3, var4, var5, var6, var7;
		Float_t var8, var9, var10, var11, var12, var13, var14;

		reader->AddVariable( "Lb_TAU",                 &var1);
		reader->AddVariable( "Pim_ETA",             &var2);
		reader->AddVariable( "Pim_PT",              &var3);
		reader->AddVariable( "Lb_VectorZ",             &var4);
		reader->AddVariable( "Lb_DTFonlyPV_chi2onDOF", &var5);

		// Book the MVA methods

		TString dir    = model;
		TString prefix = "TMVAClassification";

		// Book method(s)
		for (std::map<std::string,int>::iterator it = Use.begin(); it != Use.end(); it++) {
				if (it->second) {
						TString methodName = TString(it->first) + TString(" method");
						TString weightfile = dir + prefix + TString("_") + TString(it->first) + TString(".weights.xml");
						reader->BookMVA( methodName, weightfile );
				}
		}

		// Prepare input tree (this must be replaced by your data source)
		// in this example, there is a toy tree with signal and one with background events
		// we'll later on use only the "signal" events for the test in this example.
		//

    TFile *file = new TFile(inputfile, "READ", "");
    TTree* chain = (TTree*)file->Get("DecayTree");

		//TChain *chain=new TChain("DecayTree");
		//chain->Add(inputfile);
		std::cout << "--- TMVAClassificationApp    : Using input file: " << inputfile << std::endl;

		// Event loop

		// Prepare the event tree
		// - Here the variable names have to corresponds to your tree
		// - You can use the same variables as above which is slightly faster,
		//   but of course you can use different ones and copy the values inside the event loop
		//
 		std::cout << "--- Select signal sample" << std::endl;
    TFile *target0  = new TFile(tempfile,"RECREATE" );
		TTree* theTree = (TTree*)chain->CopyTree(cut_loose_Lb);
		//   Float_t userVar1, userVar2;

		Double_t dvar1, dvar2, dvar3, dvar4, dvar5;
		Double_t dvar8, dvar9, dvar10, dvar11, dvar12, dvar13, dvar14;

		theTree->SetBranchAddress( "Lb_TAU",                 &dvar1);
		theTree->SetBranchAddress( "Pim_ETA",             &dvar2);
		theTree->SetBranchAddress( "Pim_PT",              &dvar3);
		theTree->SetBranchAddress( "Lb_VectorZ",             &dvar4);
		theTree->SetBranchAddress( "Lb_DTFonlyPV_chi2onDOF", &dvar5);
		// Efficiency calculator for cut method
		Int_t    nSelCutsGA = 0;
		Double_t effS       = 0.7;

		std::cout << "--- Processing: " << theTree->GetEntries() << " events" << std::endl;
		TStopwatch sw;
		sw.Start();

		TFile *target  = new TFile(outputfile,"RECREATE" );
		Float_t BDT;
		//Float_t BDTB;
		//Float_t BDTD;
		//Float_t BDTF;
		//Float_t BDTG;

		TCut cut="1";
		TTree *tree= theTree->CloneTree(0);
		tree->Branch("BDT" ,&BDT ,"BDT/F");
		//tree->Branch("BDTB",&BDTB,"BDTB/F");
		//tree->Branch("BDTD",&BDTD,"BDTD/F");
		//tree->Branch("BDTF",&BDTF,"BDTF/F");
		//tree->Branch("BDTG",&BDTG,"BDTG/F");

		for (Long64_t ievt=0; ievt<theTree->GetEntries();ievt++) {
				BDT =-100;
				//BDTB=-100;
				//BDTD=-100;
				//BDTF=-100;
				//BDTG=-100;
				if (ievt%1000 == 0) std::cout << "--- ... Processing event: " << ievt << std::endl;

				theTree->GetEntry(ievt);

				var1=(double)dvar1;
				var2=(double)dvar2;
				var3=(double)dvar3;
				var4=(double)dvar4;
				var5=(double)dvar5;

				//var5=log((double)dvar5);
				//var6=log((double)dvar6);
				//var7=log((double)dvar7);
				//var8=log((double)dvar8);
				//var9=log((double)dvar9);
				//var10=log((double)dvar10);
				//var11=log((double)dvar11);
				//var12=log((double)dvar12);
				//var13=log((double)dvar13);
				//var14=log((double)dvar14);
				BDT =reader->EvaluateMVA("BDT method");
				//BDTB=reader->EvaluateMVA("BDTB method");
				//BDTD=reader->EvaluateMVA("BDTD method");
				//BDTF=reader->EvaluateMVA("BDTF method");
				//BDTG=reader->EvaluateMVA("BDTG method"); 
				tree->Fill();
		}

		// Get elapsed time
		sw.Stop();
		std::cout << "--- End of event loop: "; sw.Print();

		delete reader;

		std::cout << "==> TMVAClassificationApplication is done!" << std::endl << std::endl;

		tree->Write();
		target->Close();
}

#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "TChain.h"
#include "/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Snake/cut.h"

void writeGBRafterBDT(TString inputfile, TString outputfile, TString weightfile) {

		TFile *file = new TFile(inputfile, "READ", "");
		TTree* chain = (TTree*)file->Get("DecayTree");

		TFile *outputFile = TFile::Open(outputfile, "RECREATE");
		if (!outputFile || outputFile->IsZombie()) {
				std::cerr << "Error creating output file!" << std::endl;
				return;
		}

		TTree *inputTree = chain->CopyTree(cut_Lb_sw);

		TTree *outputTree = inputTree->CloneTree(0);
		float newVar;
		TBranch *newBranch = outputTree->Branch("GBRweight", &newVar);

		std::ifstream infile(weightfile);
		if (!infile) {
				std::cerr << "Error opening .dat file!" << std::endl;
				return;
		}

		std::vector<float> newVars;
		float value;
		while (infile >> value) {
				newVars.push_back(value);
		}
		infile.close();

		if ((double)newVars.size() != (double)inputTree->GetEntries()) {
				std::cerr << "Mismatch between .dat file length and tree length!" << std::endl;
				return;
		}

		for (int i = 0; i < inputTree->GetEntries(); ++i) {
				inputTree->GetEntry(i);
				newVar = newVars[i];
				outputTree->Fill();
		}

		outputTree->Write();
		outputFile->Close();
		delete outputFile;

		std::cout << "Processing complete. New file created!" << std::endl;
}

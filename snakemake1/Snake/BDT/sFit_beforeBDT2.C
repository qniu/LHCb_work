#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "RooFitResult.h"
#include "RooPlot.h"
#include "RooAbsData.h"
#include "RooHistPdf.h"
#include "RooAddPdf.h"
#include "RooStats/SPlot.h"
#include "TChain.h"
#include "RooHypatia2.h"
#include "RooJohnson.h"
#include "RooLinearVar.h"
#include "RooChebychev.h"
#include "RooFormulaVar.h"
#include "TAxis.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TPaveText.h"
#include "RooFFTConvPdf.h"
#include <iomanip> 
#include "/lzufs/home/wanghj/lhcbStyle.h"
#include "/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Snake/cut.h"
using namespace std;
using namespace RooFit;

//void sFit_beforeBDT2(string infile="/publicfs2/ucas/user/wanghj/LHCb/run2/snakemake/Output/addvariables/DATA_Lb2Lcpi_2016_MU_addvariables.root",TString outfile="out.root",TString pdffile="out.pdf"){
void sFit_beforeBDT2(string infile,TString outfile,TString pdffile,TString Lcpi_params, TString LcK_params, TString MC_Lcpi_file, TString MC_LcK_file){
		//RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL) ;
		int par=-1;
		if(infile.find("2016") != std::string::npos) par=0;
		if(infile.find("2017") != std::string::npos) par=2;
		if(infile.find("2018") != std::string::npos) par=4;

		double fitrange[2]={5520,5750};//5520,5850
		double xbins=50;
		double max=1200;
		TCut cutfit=Form("DTFonlyPV_Lb_M>%f&&DTFonlyPV_Lb_M<%f",fitrange[0], fitrange[1]);
		TChain * chain = new TChain("DecayTree");
		chain->Add(infile.c_str());
		TTree* tree = chain->CopyTree(cut_Lc+cut_loose_Lb+cutfit);

		RooRealVar *var = new RooRealVar("DTFonlyPV_Lb_M", "observable", fitrange[0], fitrange[1]);
		RooDataSet *data = new RooDataSet("data", "dataset", tree, *var);

		//shape_Lb
		RooRealVar *Lb_lambda = new RooRealVar("Lb_lambda", "Lb_lambda", -5.0,-10.0,0.0);
		RooRealVar *Lb_zeta   = new RooRealVar("Lb_zeta", "Lb_zeta", 0);
		RooRealVar *Lb_beta   = new RooRealVar("Lb_beta", "Lb_beta",  0, -5, 5);
		RooRealVar *Lb_sigma  = new RooRealVar("Lb_sigma", "Lb_sigma", 10, 0, 100);
		RooRealVar *Lb_mean   = new RooRealVar("Lb_mean", "Lb_mean", 5619, 5600, 5640);
		RooRealVar *Lb_a      = new RooRealVar("Lb_a", "Lb_a", 1.0, 0, 3); 
		RooRealVar *Lb_n      = new RooRealVar("Lb_n", "Lb_n", 1.0, 0, 3);
		RooRealVar *Lb_a2     = new RooRealVar("Lb_a2", "Lb_a2", 1.0, 0, 3); 
		RooRealVar *Lb_n2     = new RooRealVar("Lb_n2", "Lb_n2", 1.0, 0, 3);
		RooHypatia2 *Lb_Hypatia2_ori = new RooHypatia2("Lb_Hypatia2_ori","Lb_Hypatia2_ori", *var,*Lb_lambda,*Lb_zeta,*Lb_beta,*Lb_sigma,*Lb_mean,*Lb_a,*Lb_n,*Lb_a2,*Lb_n2);
		//Read initial params
		auto Lb_h_params = Lb_Hypatia2_ori -> getParameters(*var);
		Lb_h_params->readFromFile(Lcpi_params);
		Lb_h_params->Print("v");

		//shape_LcK
		RooRealVar *LcK_lambda = new RooRealVar("LcK_lambda", "LcK_lambda", -5.0,-10.0,0.0);
		RooRealVar *LcK_zeta   = new RooRealVar("LcK_zeta", "LcK_zeta", 0);
		RooRealVar *LcK_beta   = new RooRealVar("LcK_beta", "LcK_beta",  0, -5, 5);
		RooRealVar *LcK_sigma  = new RooRealVar("LcK_sigma", "LcK_sigma", 10, 0, 100);
		RooRealVar *LcK_mean   = new RooRealVar("LcK_mean", "LcK_mean", 5619, 5600, 5640);
		RooRealVar *LcK_a      = new RooRealVar("LcK_a", "LcK_a", 1.0, 0, 3); 
		RooRealVar *LcK_n      = new RooRealVar("LcK_n", "LcK_n", 1.0, 0, 3);
		RooRealVar *LcK_a2     = new RooRealVar("LcK_a2", "LcK_a2", 1.0, 0, 3); 
		RooRealVar *LcK_n2     = new RooRealVar("LcK_n2", "LcK_n2", 1.0, 0, 3);
		RooHypatia2 *LcK_Hypatia2_ori = new RooHypatia2("LcK_Hypatia2_ori","LcK_Hypatia2_ori", *var,*LcK_lambda,*LcK_zeta,*LcK_beta,*LcK_sigma,*LcK_mean,*LcK_a,*LcK_n,*LcK_a2,*LcK_n2);
		//Read initial params
		auto LcK_h_params = LcK_Hypatia2_ori -> getParameters(*var);
		LcK_h_params->readFromFile(LcK_params);
		LcK_h_params->Print("v");

		double ssigma=0.1;
		//if(par==4) ssigma=20;

		RooRealVar *mean = new RooRealVar("mean", "mean", 0.1, -10, 10);
		RooRealVar *sigma  = new RooRealVar("sigma", "sigma", ssigma, -20, 50);
		RooGaussian *gaussian=new RooGaussian("gaussian","gaussian",*var,*mean,*sigma);

		RooFFTConvPdf *Lb_Hypatia2  = new RooFFTConvPdf("Lb_Hypatia2","Lb x resolution",*var,*Lb_Hypatia2_ori,*gaussian);
		RooFFTConvPdf *LcK_Hypatia2 = new RooFFTConvPdf("LcK_Hypatia2","LcK x resolution",*var,*LcK_Hypatia2_ori,*gaussian);

		//shape_Lbbkg
		RooRealVar *Lbbkg_c0 = new RooRealVar("Lbbkg_c0", "Lbbkg_c0", -1.64027e-01, -1.0, 0.0);
		RooChebychev *Lbbkg_chey = new RooChebychev("Lbbkg_chey", "Lbbkg_chey", *var, RooArgList(*Lbbkg_c0));

		TChain * chain_Lcpi = new TChain("DecayTree");
		chain_Lcpi->Add(MC_Lcpi_file);
		TChain * chain_LcK = new TChain("DecayTree");
		chain_LcK->Add(MC_LcK_file);

		double Eff_acc_Lcpi[6]={0.1926,0.1913,0.1939,0.1922,0.1941,0.1915};
		double Eff_acc_LcK[6] ={0.1981,0.1995,0.1985,0.1996,0.1942,0.1945};
		double Racc = (Eff_acc_LcK[par]+Eff_acc_LcK[par+1])/(Eff_acc_Lcpi[par]+Eff_acc_Lcpi[par+1]);
		double Ratio=(double)chain_LcK->GetEntries(cut_Lc+cutfit)/(double)chain_Lcpi->GetEntries(cut_Lc+cutfit)*Racc*3.56e-4/4.9e-3;

		cout<<"0.01562   "<<Ratio<<endl;

    RooRealVar   nsig("nsig", "", 10000, 0, 5e05);
    RooLinearVar nmis("nmis","", nsig, RooConst(Ratio), RooConst(0));
    RooRealVar   nbkg("nbkg", "", 5.0e03, 0, 5e04);

		RooArgList shapes, yields;
		shapes.add(*Lb_Hypatia2);       yields.add(nsig);
		shapes.add(*LcK_Hypatia2);     yields.add(nmis);
		shapes.add(*Lbbkg_chey);        yields.add(nbkg);

		RooAddPdf *model = new RooAddPdf("model", "model",  shapes, yields);

		RooFitResult* fitResult = model->fitTo(*data, Extended(kTRUE), RooFit::Save());

		fitResult->Print("v");

		//draw
		lhcbStyle(); 
		gStyle->SetOptStat(0);
		gStyle->SetTitleOffset(1.2,"Y");
		gStyle->SetTitleOffset(1.1,"Y");

		gStyle->SetPadLeftMargin(0.16); // increase for colz plots
		gStyle->SetPadRightMargin(0.05); // increase for colz plots
		gStyle->SetPadBottomMargin(0.16);
		gStyle->SetPaintTextFormat("3.2f");

		RooPlot *frame = var->frame(fitrange[0], fitrange[1], xbins);
		data->plotOn(frame, RooFit::Name("data"), MarkerSize(1.1));
		model->plotOn(frame,RooFit::Name("model"), LineColor(kGreen+3), LineWidth(3), LineStyle(1));
		RooHist *pull = frame -> pullHist();
		model->plotOn(frame,RooFit::Name("sig"), Components(RooArgSet(*Lb_Hypatia2)),LineColor(kOrange-3),  LineWidth(3),LineStyle(1));
		model->plotOn(frame,RooFit::Name("mis"), Components(RooArgSet(*LcK_Hypatia2)),LineColor(kMagenta+1), LineWidth(3),LineStyle(9));
		model->plotOn(frame,RooFit::Name("bkg"), Components(RooArgSet(*Lbbkg_chey)),LineColor(kAzure+3), LineWidth(3),LineStyle(9));
		data->plotOn(frame, RooFit::Name("data"), MarkerSize(1.1));

		//Pull
		auto frame_pull = var -> frame(fitrange[0],fitrange[1], xbins);

		RooConstVar F_zero("F_zero", "F_zero", 0);
		RooConstVar F_up("F_up", "F_up", 3);
		RooConstVar F_down("F_down", "F_down", -3);

		F_zero.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kBlue));
		F_up.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));
		F_down.plotOn(frame_pull, RooFit::LineStyle(2), RooFit::LineWidth(2), RooFit::LineColor(kAzure + 4));

		frame_pull->addPlotable(pull, "P");

		TCanvas *can = new TCanvas("can", "can", 800, 600);
		can -> Divide(1,2);
		can->SetLeftMargin(0.01);
		can->SetRightMargin(0.01);
		can->SetTopMargin(0.01);
		can->SetBottomMargin(0.0);

		can -> cd(1) -> SetPad(0.0, 0.0, 1.0, 0.25);
		gPad->SetTopMargin(0.00);
		gPad->SetBottomMargin(0.60);
		gPad->SetLeftMargin(0.12);
		//gPad->SetLogy();
		frame_pull->SetMaximum(7);
		frame_pull->SetMinimum(-7);
		frame_pull->GetYaxis()->SetNdivisions(104);
		frame_pull->GetYaxis()->SetLabelSize(0.15);    
		frame_pull->GetYaxis()->CenterTitle(true);
		frame_pull->GetYaxis()->SetTitle("Pull");
		frame_pull->GetYaxis()->SetTitleSize(0.20);
		frame_pull->GetYaxis()->SetTitleOffset(0.29);
		frame_pull->GetXaxis()->SetNdivisions(505);
		frame_pull->GetXaxis()->SetLabelSize(0.2);
		//frame_pull->GetXaxis()->CenterTitle(true);
		frame_pull->GetXaxis()->SetTitleSize(0.22);
		frame_pull->GetXaxis()->SetTitleOffset(0.85);
		frame_pull->GetXaxis()->SetTitleFont(22);
		frame_pull->GetYaxis()->SetTitleFont(22);
		frame_pull->GetXaxis()->SetLabelFont(22);
		frame_pull->GetYaxis()->SetLabelFont(22);
		frame_pull->GetXaxis()->SetTitle("M(#it{#Lambda_{c}^{+}#pi^{-}}) (MeV/#it{c}^{2})");
		frame_pull->Draw();


		can -> cd(2) -> SetPad(0.0, 0.25, 1.0, 1.0);
		gPad->SetTopMargin(0.02);
		gPad->SetBottomMargin(0.00);
		gPad->SetLeftMargin(0.12);

		TString a("Events/");char b[20];sprintf(b, "(%1.1f",(fitrange[1]-fitrange[0])/xbins); TString c(" MeV/#it{c}^{2})"); TString ytitle = a + b + c;

		frame->Draw();
		frame->SetMaximum(max);
		frame->SetMinimum(0.0001);
		frame->GetXaxis()->SetTitleFont(22);
		frame->GetYaxis()->SetTitleFont(22);
		frame->GetXaxis()->SetLabelFont(22);
		frame->GetYaxis()->SetLabelFont(22);
		frame->GetXaxis()->SetTitle("M(#it{#Lambda_{c}^{+}#pi^{-}}) (MeV/#it{c}^{2})");
		frame->GetYaxis()->SetTitle(ytitle);
		frame->GetXaxis()->SetLabelOffset(0.02);
		frame->GetXaxis()->SetLabelSize(0.05);
		frame->GetXaxis()->SetTitleSize(0.06);
		frame->GetXaxis()->SetTitleOffset(1.1);
		frame->GetYaxis()->CenterTitle(true);
		frame->GetYaxis()->SetLabelOffset(0.015);
		frame->GetYaxis()->SetLabelSize(0.065);
		frame->GetYaxis()->SetTitleSize(0.065);
		frame->GetYaxis()->SetTitleOffset(0.90);

		TLegend *leg = new TLegend(0.1729323,0.6545139,0.5413534,0.9357639,NULL,"brNDC");
		leg->AddEntry(frame->findObject("data"),     "Data     ",  "lep");
		leg->AddEntry(frame->findObject("model"),    "Fit result",   "l");
		leg->AddEntry(frame->findObject("sig"),      "Signal",       "l");
		leg->AddEntry(frame->findObject("mis"),      "Mis",          "l");
		leg->AddEntry(frame->findObject("bkg"),      "Bkg",          "l");
		leg->SetFillColor(0);
		leg->SetFillStyle(0);
		leg->SetBorderSize(0);
		leg->SetLineColor(1);
		leg->SetLineStyle(1);
		leg->SetLineWidth(1);
		leg->SetTextSize(0.05);
		leg->Draw("SAME");
		TPaveText *pt = new TPaveText(0.6867168,0.7638889,0.9210526,0.9079861,"blNDC");
		pt->SetBorderSize(0);
		pt->SetFillColor(0);
		pt->SetFillStyle(0);
		pt->SetTextSize(0.06);
		pt->SetLineColor(5);
		pt->SetTextFont(22);

		TString text[6]={"DATA@2016", "DATA@2016", "DATA@2017", "DATA@2017", "DATA@2018", "DATA@2018"};
		pt->AddText(text[par]);
		pt->Draw("SAME");

		can->SaveAs(pdffile);

		Lb_lambda->setConstant(kTRUE);
		Lb_zeta->setConstant(kTRUE);
		Lb_beta->setConstant(kTRUE);
		Lb_sigma->setConstant(kTRUE);
		Lb_mean->setConstant(kTRUE);
		Lb_a->setConstant(kTRUE);
		Lb_n->setConstant(kTRUE);
		Lb_a2->setConstant(kTRUE);
		Lb_n2->setConstant(kTRUE);

		LcK_lambda->setConstant(kTRUE);
		LcK_zeta->setConstant(kTRUE);
		LcK_beta->setConstant(kTRUE);
		LcK_sigma->setConstant(kTRUE);
		LcK_mean->setConstant(kTRUE);
		LcK_a->setConstant(kTRUE);
		LcK_n->setConstant(kTRUE);
		LcK_a2->setConstant(kTRUE);
		LcK_n2->setConstant(kTRUE);

		mean->setConstant(kTRUE);
		sigma->setConstant(kTRUE);

		Lbbkg_c0->setConstant(kTRUE);

		cout<<"nmis = "<<nmis.getVal()<<endl;
		cout<<"nsig = "<<nsig.getVal()<<endl;
		cout<<"nbkg = "<<nbkg.getVal()<<endl;

		var->setRange("sig_region", 5550, 5700);
		double NN1=nsig.getVal()*Lb_Hypatia2->createIntegral(*var, *var, "sig_region")->getVal();
		double NN2=nbkg.getVal()*Lbbkg_chey->createIntegral(*var, *var, "sig_region")->getVal();
		double NN3=nmis.getVal()*LcK_Hypatia2->createIntegral(*var, *var, "sig_region")->getVal();
		cout<<"RainbowNsig="<<fixed<<setprecision(4)<<NN1<<endl;
		cout<<"RainbowNbkg="<<fixed<<setprecision(4)<<NN2<<endl;
		cout<<"RainbowNmis="<<fixed<<setprecision(4)<<NN3<<endl;

		RooRealVar ratio("ratio","ratio", nmis.getVal()/nsig.getVal());
		RooFormulaVar nMis("nMis","@0*@1",RooArgSet(nsig,ratio));
		RooAddPdf *com =new RooAddPdf("com", "bkg+mis",RooArgList(*Lbbkg_chey,*LcK_Hypatia2),RooArgList(nbkg.getVal(),nMis.getVal()));

		RooRealVar nSig("nSig","nSig",2000,0,100000.);
		RooRealVar nCom("nCom","nCom",2000,0,100000.);
		RooAddPdf *sw_model =new RooAddPdf("sw_model", "sw_model",RooArgList(*Lb_Hypatia2,*com),RooArgList(nSig,nCom));

		RooStats::SPlot* sData = new RooStats::SPlot("sData", "sData", *data, sw_model, RooArgList(nSig, nCom));
		sData->Print("v");

		TFile* outFile = TFile::Open(outfile, "RECREATE");
		TTree* weightTree = tree->CloneTree(0);
		double sig_sw, bkg_sw, mis_sw;
		weightTree->Branch("sig_sw", &sig_sw, "sig_sw/D");
		weightTree->Branch("bkg_sw", &bkg_sw, "bkg_sw/D");

		float Lb_m;
		tree->SetBranchAddress("DTFonlyPV_Lb_M",&Lb_m);

		int index=0;
		for(int i=0;i<tree->GetEntries();i++){
				tree->GetEntry(i);
				index++;
				const RooArgSet *row = data->get(index-1);
				RooRealVar *Mass_row = (RooRealVar*)row->find("DTFonlyPV_Lb_M");
				RooRealVar *mass_nSig_sw_row = (RooRealVar*)row->find("nSig_sw");
				RooRealVar *mass_nCom_sw_row = (RooRealVar*)row->find("nCom_sw");
				if(Lb_m != Mass_row->getVal()){
						cout<<__LINE__<<"!!! ERROR : "<< Lb_m << ", " << Mass_row->getVal()<<endl;
						break;
				}
				sig_sw = mass_nSig_sw_row->getVal();
				bkg_sw = mass_nCom_sw_row->getVal();
				weightTree->Fill();
		}

		weightTree->Write();
		outFile->Close();
		delete fitResult;
		delete sData;
}

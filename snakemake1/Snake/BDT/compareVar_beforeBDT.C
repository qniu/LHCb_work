#include "TChain.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include "/lzufs/home/niuql/mytools.h"
#include "/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Snake/DrawStyle_Root6.h" 
void compareVar_beforeBDT(TString rootfile_MC, TString rootfile_data){
    gStyle->SetFrameBorderMode(0);
    gStyle->SetCanvasBorderMode(0);
    gStyle->SetPadBorderMode(0);
    gStyle->SetPadColor(0);
    gStyle->SetCanvasColor(0);
    gStyle->SetStatColor(0);
    gStyle->SetTitleFillColor(0);

    // set the paper & margin sizes
    gStyle->SetPaperSize(20,26);
    gStyle->SetPadTopMargin(0.05);
    gStyle->SetPadRightMargin(0.05);
    gStyle->SetPadBottomMargin(0.16);
    gStyle->SetPadLeftMargin(0.12);

    // use large Times-Roman fonts
    gStyle->SetTitleFont(22,"xyz");  // set the all 3 axes title font
    gStyle->SetTitleFont(22," ");    // set the pad title font
    gStyle->SetTitleSize(0.06,"xyz"); // set the 3 axes title size
    gStyle->SetTitleSize(0.06," ");   // set the pad title size
    gStyle->SetLabelFont(22,"xyz");
    gStyle->SetLabelSize(0.06,"xyz");
    gStyle->SetTextFont(22);
    gStyle->SetTextSize(0.08);
    gStyle->SetStatFont(22);

    // use bold lines and markers
    gStyle->SetMarkerStyle(8);
    gStyle->SetMarkerSize(0.8);
    //gStyle->SetHistLineWidth(1.85);
    gStyle->SetLineStyleString(2,"[12 12]");// postscript dashes

    //..Get rid of X error bars
    gStyle->SetErrorX(0.001);

    // do not display any of the standard histogram decorations
    gStyle->SetOptTitle(0);
    gStyle->SetOptStat(0);
    gStyle->SetOptFit(0);

    // put tick marks on top and RHS of plots
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);

    gStyle->SetOptFit(0);                                                                                              

    // put tick marks on top and RHS of plots
    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);

    //setStyle();myStyle->SetTitleXSize(0.08);
    //myStyle->SetTitleYSize(0.07);
    gStyle->SetNdivisions(508,"X");
    gStyle->SetNdivisions(508,"Y");
    gStyle->SetLabelFont(22,"X");
    gStyle->SetLabelFont(22,"Y");
    gStyle->SetTitleFont(22,"X");
    gStyle->SetTitleFont(22,"Y");
    //gStyle->SetLabelSize(0.05,"X");
    gStyle->SetLabelSize(0.0,"X");
    gStyle->SetLabelSize(0.05,"Y");
    //gStyle->SetTitleXSize(0.07);
    gStyle->SetTitleXSize(0.0);
    gStyle->SetTitleYSize(0.06);
    gStyle->SetTitleXOffset(0.9); 
    gStyle->SetTitleYOffset(1.0); 
    // ---------------------------------------

    TChain *tr_MC = new TChain("DecayTree");
    tr_MC->Add(rootfile_MC);
    TChain *tr_data = new TChain("DecayTree");
    tr_data->Add(rootfile_data);

    TCut cut_MC = "1*GBRweight";
    TCut cut_data = "1*sig_sw";

    TCanvas *cs = new TCanvas("cs", "cs", 800, 600);
    cs->SetFillColor(0);
    cs->SetFrameFillColor(0);
    cs->SetLeftMargin(0.14);
    cs->SetRightMargin(0.07);
    cs->SetTopMargin(0.07);
    cs->Divide(1,2);

    double xmin[45]={-8, -9,  6.5,  9, -1,   2, 2 , 0,  -4, 8,  0.4, -2, -5,   6, 9 , -17, -2, -4,  0, -5, 5,   7.5, 0.5, 0,  -5,   4,  7,  0.5,  0, -4,  4.5,     7, 0.5,   0, 0   , 0   , -1  , 0   , -1 , 1.5 ,0    ,  -10, -100, 0};
    double xmax[45]={ 6,  0, 11.5, 14,  6,  15, 14, 13,  5, 14, 1.8, 14,  3, 11.5, 14,  -2,  6,  9, 12,  3, 11, 13.5, 1.7, 11,  4, 11, 13,  1.7, 11,  3,   10,  12.5, 1.7, 800, 1000, 3.15,  7.3, 3.15, 7.3, 5.5 ,25000,  -3 , 20  ,10};

    TString variables[45] = {
        "log(Lb_IPCHI2_OWNPV)",
        "log(Lb_IP_OWNPV)",
        "log(Lb_PT)",
        "log(Lb_P)",
        "log(Lb_FD_OWNPV)",
        "log(Lb_FDCHI2_OWNPV)",
        "log(Lc_FDCHI2_OWNPV)",
        "log(Pim_IPCHI2_OWNPV)",
        "log(Pim_IP_OWNPV)",
        "log(Pim_P)",
        "log(Pim_ETA)",
        "log(Lc_IPCHI2_OWNPV)",
        "log(Lc_IP_OWNPV)",
        "log(Lc_PT)",
        "log(Lc_P)",
        "log(Lc_TAU)",
        "log(Lc_FD_OWNPV)",
        "log(Lc_ENDVERTEX_Z)",
        "log(Pp_IPCHI2_OWNPV)",
        "log(Pp_IP_OWNPV)",
        "log(Pp_PT)",
        "log(Pp_P)",
        "log(Pp_ETA)",
        "log(Kp_IPCHI2_OWNPV)",
        "log(Kp_IP_OWNPV)",
        "log(Kp_PT)",
        "log(Kp_P)",
        "log(Kp_ETA)",
        "log(Km_IPCHI2_OWNPV)",
        "log(Km_IP_OWNPV)",
        "log(Km_PT)",
        "log(Km_P)",
        "log(Km_ETA)",
        "nTracks",
        "nSPDHits",
        "theta0",
        "phi1",
        "theta1",
        "phi2",
	"Lb_ETA",
	"Pim_PT",
	"log(Lb_TAU)",
	"Lb_VectorZ",
	"Lb_DTFonlyPV_chi2onDOF",

    };


    TString titles[45] = {
        "log(Lb_IPCHI2_OWNPV)",
        "log(Lb_IP_OWNPV)",
        "log(Lb_PT)",
        "log(Lb_P)",
        "log(Lb_FD_OWNPV)",
        "log(Lb_FDCHI2_OWNPV)",
        "log(Lc_FDCHI2_OWNPV)",
        "log(Pim_IPCHI2_OWNPV)",
        "log(Pim_IP_OWNPV)",
        "log(Pim_P)",
        "log(Pim_ETA)",
        "log(Lc_IPCHI2_OWNPV)",
        "log(Lc_IP_OWNPV)",
        "log(Lc_PT)",
        "log(Lc_P)",
        "log(Lc_TAU)",
        "log(Lc_FD_OWNPV)",
        "log(Lc_ENDVERTEX_Z)",
        "log(Pp_IPCHI2_OWNPV)",
        "log(Pp_IP_OWNPV)",
        "log(Pp_PT)",
        "log(Pp_P)",
        "log(Pp_ETA)",
        "log(Kp_IPCHI2_OWNPV)",
        "log(Kp_IP_OWNPV)",
        "log(Kp_PT)",
        "log(Kp_P)",
        "log(Kp_ETA)",
        "log(Km_IPCHI2_OWNPV)",
        "log(Km_IP_OWNPV)",
        "log(Km_PT)",
        "log(Km_P)",
        "log(Km_ETA)",
        "nTracks",
        "nSPDHits",
        "#theta_{0}",
        "#phi_{1}",
        "#theta_{1}",
        "#phi_{2}",
	"Lb_ETA",
	"Pim_PT",
	"log(Lb_TAU)",
	"Lb_VectorZ",
	"Lb_DTFonlyPV_chi2onDOF",
    };
    int count = 0;

    for(int i=0; i<44; i++){

        cs->cd(1); 
        gPad->SetPad(0.0,0.13,1.0,1.0);

        TString hist_1 = Form("h1_%d",count); //data
        TString hist_2 = Form("h2_%d",count); //GBR MC
        TString hist_3 = Form("h3_%d",count); //GBR MC

        tr_MC->Draw(variables[i] + ">>" + hist_1+"(80,"+xmin[i]+"," +xmax[i]+")", "", "HIST");
        tr_MC->Draw(variables[i] + ">>" + hist_2+"(80,"+xmin[i]+"," +xmax[i]+")", cut_MC, "HIST SAME");
        tr_data->Draw(variables[i] + ">>" + hist_3+"(80,"+xmin[i]+"," +xmax[i]+")", cut_data, "HIST SAME");
        TH1F	*th1f_data_ratio=new TH1F("th1f_data_ratio","", 80, xmin[i], xmax[i]); th1f_data_ratio->SetMinimum(0);
        auto h1 = (TH1F*)gPad->GetPrimitive(hist_1);
        auto h2 = (TH1F*)gPad->GetPrimitive(hist_2);
        auto h3 = (TH1F*)gPad->GetPrimitive(hist_3);

        auto h1_smoothed = new TH1F(*h1);
        setTH1(h1_smoothed);
        //h1_smoothed->GetXaxis()->SetTitle(titles[i]);
        h1_smoothed->GetXaxis()->SetLabelSize(0);
        h1_smoothed->SetLineColor(kBlue);
        h1_smoothed->SetMarkerColor(kBlue);
        h1_smoothed->SetMinimum(0);
        h1_smoothed->SetLineWidth(3);
        h1_smoothed->Smooth();

        auto h2_smoothed = new TH1F(*h2);
        setTH1(h2_smoothed);
        h2_smoothed->GetXaxis()->SetTitle(titles[i]);
        h2_smoothed->SetLineColor(kRed);
        h2_smoothed->SetMarkerColor(kRed);
        h2_smoothed->SetMinimum(0);
        h2_smoothed->SetLineWidth(3);
        h2_smoothed->Smooth();

        auto h3_smoothed = new TH1F(*h3);
        setTH1(h3_smoothed);
        //h3_smoothed->GetXaxis()->SetTitle(titles[i]);
        h3_smoothed->SetLineColor(kBlack);
        h3_smoothed->SetMarkerColor(kBlack);
        h3_smoothed->GetXaxis()->SetLabelSize(0);
        h3_smoothed->SetMinimum(0);
        h3_smoothed->SetLineWidth(3);
        h3_smoothed->Smooth();

        double sc1 = h1_smoothed->Integral();
        double sc2 = h2_smoothed->Integral();
        double sc3 = h3_smoothed->Integral();

        h1_smoothed->Scale(sc2/sc1);
        h3_smoothed->Scale(sc2/sc3);
        th1f_data_ratio->Divide(h1_smoothed,h2_smoothed);
	if(i>=41)th1f_data_ratio->Divide(h3_smoothed,h2_smoothed);

        //h1_smoothed->Scale(1/1.15);

        h3_smoothed->Draw("H");
        h1_smoothed->Draw("H SAME");
        h2_smoothed->Draw("H SAME");

        TLegend *leg = new TLegend(0.6290727,0.7126437,0.8984962,0.9042146,NULL,"brNDC");
        leg->AddEntry(h1_smoothed,"#bf{MC origin}","lep");
        leg->AddEntry(h2_smoothed,"#bf{MC weight}","lep");
        leg->AddEntry(h3_smoothed,"#bf{sweighted data}","lp");
        leg->SetNColumns(1);
        leg->SetFillStyle(0);
        leg->SetNColumns(1);
        leg->SetBorderSize(0);
        leg->SetFillColor(0);
        leg->SetTextSize(0.04285714);
        leg->Draw();

        cs->cd(2);
        gPad->SetPad(0.0,0.01,1.0,0.22);
        gPad->SetBottomMargin(0.4) ;
        gPad->SetTopMargin(0) ;

        th1f_data_ratio->GetXaxis()->SetTitle(titles[i]);
        th1f_data_ratio->GetXaxis()->SetNdivisions(508,"X");
        th1f_data_ratio->GetYaxis()->SetNdivisions(2,kTRUE);
        th1f_data_ratio->GetYaxis()->SetLabelSize(0.18);
        th1f_data_ratio->GetXaxis()->SetLabelSize(0.18);
        th1f_data_ratio->GetYaxis()->SetTitle("ratio");
        th1f_data_ratio->GetXaxis()->SetTitleOffset(0.93) ;
        th1f_data_ratio->GetYaxis()->SetTitleOffset(0.2);
        th1f_data_ratio->GetXaxis()->SetTitleSize(0.2) ;
        th1f_data_ratio->GetYaxis()->SetTitleSize(0.2) ;
        th1f_data_ratio->SetMinimum(-1);
        th1f_data_ratio->SetMaximum(3);

        th1f_data_ratio->Draw("E");
        th1f_data_ratio->SetMarkerStyle(50);
        th1f_data_ratio->SetMarkerSize(1);

        TLine *linemid2 = new TLine(xmin[i],1,xmax[i],1);
        linemid2->SetLineColor(kBlue+2);
        linemid2->SetLineStyle(kDashed);
        linemid2->Draw("same"); 

TString VAR1;
	if (rootfile_MC.Contains("2016")) {
        VAR1 = Form("/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Output/compareVar_beforeBDT/2016/%s.png", titles[i].Data());
}
	if (rootfile_MC.Contains("2017")) {
        VAR1 = Form("/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Output/compareVar_beforeBDT/2017/%s.png", titles[i].Data());
}
	if (rootfile_MC.Contains("2018")) {
        VAR1 = Form("/lzufs/user/niuql/Analysis/Lmdb2Lmdcpi_Lmdc2PKK/SnakeMake_for_Auto_2/snakemake1/Output/compareVar_beforeBDT/2018/%s.png", titles[i].Data());
}
std::cout << "titles[i]: " << titles[i] << std::endl;
std::cout << "titles[i].Data(): " << titles[i].Data() << std::endl;
        //cs->Update();
	cs->SaveAs(VAR1);
	
        count++;
	delete h1_smoothed;
	delete h2_smoothed;
	delete h3_smoothed;
    }

}

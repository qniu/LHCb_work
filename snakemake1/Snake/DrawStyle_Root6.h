#include "TStyle.h"
#include "TPad.h"
#include "iostream"
#include "TH1.h"
#include "TH2.h"
#include "RooPlot.h"
#include "THStack.h"
void setStyle(){

	gStyle->SetPalette(1);
	gStyle->SetFillColor(0);
	gStyle->SetOptFit(0);             //1= display fitted result, 0 = no
	gStyle->SetOptTitle(0);             //1= display fitted result, 0 = no
	gStyle->SetOptStat(0);            //1= display the statistics, 0 = no
	gStyle->SetTitleFont(22);         // Font for X, Y, and Top title
	gStyle->SetNdivisions(505,"X");
	gStyle->SetNdivisions(505,"Y");

	gStyle->SetPadLeftMargin(0.2);         // pad left margin  for writing Y title
	gStyle->SetPadBottomMargin(0.22);       // pad bottom margin for writing X title
	gStyle->SetPadRightMargin(0.04);       // 0.04 pad right margin  for writing Y title
	gStyle->SetPadTopMargin(0.06);         // 0.04 pad top margin for writing X title

	gStyle->SetMarkerSize(0.8);
	gStyle->SetStatFont(22);          // Font for statistics
	gStyle->SetLabelFont(22,"X");     // Font for X label
	gStyle->SetLabelFont(22,"Y");     // Font for Y label
	gStyle->SetTitleFont(22,"X");     // Font for X title 
	gStyle->SetTitleFont(22,"Y");     // Font for X title 
	gStyle->SetLabelSize(0.06,"X");   // Size for X label
	gStyle->SetLabelSize(0.06,"Y");   // Size for Y label
	gStyle->SetLabelOffset(0.015,"X");   // Size for X label
	gStyle->SetLabelOffset(0.015,"Y");   // Size for Y label
	gStyle->SetTitleXOffset(1.);     // X title offset
	gStyle->SetTitleYOffset(1.);     // Y title offset
	gStyle->SetTitleXSize(0.08);      // X title size
	gStyle->SetTitleYSize(0.08);      // Y title size
	gStyle->SetHistLineWidth(0);      // Histogram line width
	gStyle->SetStatX(0.95);           // Centroid X position of statistics box
	gStyle->SetStatY(0.95);           // Centroid Y position of statistics box
	gStyle->SetTitleX(0.2);          // Centroid X position of title box
	gStyle->SetTitleY(0.2);          // Centroid Y position of title box
}

void setPad()
{
  gStyle->SetOptStat(0);
	//gPad->SetGrid();
	//gPad->SetTicks(1,1);//set right Y and top X ticks 
	//gPad->SetTicky(1);
	gPad->SetBorderMode(0);
	gPad->SetBorderSize(0);
	gPad->SetFrameFillColor(0);
	gPad->SetFrameBorderMode(0);
	gPad->SetFrameLineWidth(2);
	gPad->SetFillColor(0);
	gPad->SetLeftMargin(0.185);         // pad left margin  for writing Y title
	gPad->SetBottomMargin(0.19);       // pad bottom margin for writing X title
	gPad->SetRightMargin(0.05);//(0.04);       // pad right margin  for writing Y title
	gPad->SetTopMargin(0.06);         // pad top margin for writing X title
	//gPad->SetLogy(1);  
	//gPad->SetRightMargin(0.003);       // pad right margin  for writing Y title
	//gPad->SetLeftMargin(0.22);       // pad right margin  for writing Y title
}

void setPad(TVirtualPad* pad)
{
	//pad->SetGrid();
	pad->SetBorderMode(0);
	pad->SetBorderSize(0);
	pad->SetFrameFillColor(0);
	pad->SetFrameBorderMode(0);
	pad->SetFillColor(0);
	pad->SetLeftMargin(0.165);         // pad left margin  for writing Y title
	pad->SetBottomMargin(0.17);       // pad bottom margin for writing X title
	pad->SetRightMargin(0.05);//(0.04);       // pad right margin  for writing Y title
	pad->SetTopMargin(0.06);         // pad top margin for writing X title
//	pad->SetLogy(1);  
//	pad->SetRightMargin(0.003);       // pad right margin  for writing Y title
//	pad->SetLeftMargin(0.22);       // pad right margin  for writing Y title
}

void set2DPad()
{
	//gPad->SetGrid();
	gPad->SetBorderMode(0);
	gPad->SetBorderSize(0);
	gPad->SetFrameFillColor(0);
	gPad->SetFrameBorderMode(0);
	gPad->SetFillColor(0);
	gPad->SetLeftMargin(0.165);         // pad left margin  for writing Y title
	gPad->SetBottomMargin(0.17);       // pad bottom margin for writing X title
	gPad->SetRightMargin(0.13);//(0.04);       // pad right margin  for writing Y title
	gPad->SetTopMargin(0.06);         // pad top margin for writing X title
	//gPad->SetLogy(1);  
	//gPad->SetRightMargin(0.003);       // pad right margin  for writing Y title
	//gPad->SetLeftMargin(0.22);       // pad right margin  for writing Y title
}

void set2DPad(TVirtualPad* pad)
{
	//pad->SetGrid();
	pad->SetBorderMode(0);
	pad->SetBorderSize(0);
	pad->SetFrameFillColor(0);
	pad->SetFrameBorderMode(0);
	pad->SetFillColor(0);
	pad->SetLeftMargin(0.165);         // pad left margin  for writing Y title
	pad->SetBottomMargin(0.17);       // pad bottom margin for writing X title
	pad->SetRightMargin(0.13);//(0.04);       // pad right margin  for writing Y title
	pad->SetTopMargin(0.06);         // pad top margin for writing X title
	//pad->SetLogy(1);  
	//pad->SetRightMargin(0.003);       // pad right margin  for writing Y title
	//pad->SetLeftMargin(0.22);       // pad right margin  for writing Y title
}

void set3DPad()
{
	//gPad->SetGrid();
	gPad->SetBorderMode(0);
	gPad->SetBorderSize(0);
	gPad->SetFrameFillColor(0);
	gPad->SetFrameBorderMode(0);
	gPad->SetFillColor(0);
	gPad->SetLeftMargin(0.1);         // pad left margin  for writing Y title
	gPad->SetBottomMargin(0.1);       // pad bottom margin for writing X title
	gPad->SetRightMargin(0.04);//(0.04);       // pad right margin  for writing Y title
	gPad->SetTopMargin(0.04);         // pad top margin for writing X title
	//gPad->SetLogy(1);  
	//gPad->SetRightMargin(0.003);       // pad right margin  for writing Y title
	//gPad->SetLeftMargin(0.22);       // pad right margin  for writing Y title
}

void setTH1(TH1* h){

	h->SetFillColor(0);
	//h->SetLineColor(1);
	h->SetLineWidth(2);
	h->SetTitleFont(132); //default 42  time bold 22 time 132        // Font for X, Y, and Top title
	//h->SetTitleFont(22);         // Font for X, Y, and Top title
	h->SetNdivisions(509,"X");
	h->SetNdivisions(509,"Y");

	h->SetMarkerSize(0.8);
	h->SetLabelFont(132,"X");     // Font for X label
	h->SetLabelFont(132,"Y");     // Font for Y label
	h->SetTitleFont(132,"X");     // Font for X title 
	h->SetTitleFont(132,"Y");     // Font for X title 
	//h->SetLabelFont(22,"X");     // Font for X label
	//h->SetLabelFont(22,"Y");     // Font for Y label
	//h->SetTitleFont(22,"X");     // Font for X title 
	//h->SetTitleFont(22,"Y");     // Font for X title 
	h->SetLabelSize(0.06,"X");   // Size for X label
	h->SetLabelSize(0.06,"Y");   // Size for Y label
	h->SetLabelOffset(0.015,"X");   // Size for X label
	h->SetLabelOffset(0.015,"Y");   // Size for Y label
	//h->GetXaxis()->CenterTitle(true);
	//h->GetYaxis()->CenterTitle(true);
	h->GetXaxis()->SetTickLength(0.02);
	h->GetYaxis()->SetTickLength(0.02);
	h->GetZaxis()->SetTickLength(0.02);
	h->GetXaxis()->SetTitleOffset(1.);     // X title offset
	h->GetYaxis()->SetTitleOffset(1.);     // Y title offset
	h->GetXaxis()->SetTitleSize(0.08);      // X title size
	h->GetYaxis()->SetTitleSize(0.08);      // Y title size
}

void setTH2(TH2* hist){
	hist->SetFillColor(0);
	//hist->SetTitleFont(22);         // Font for X, Y, and Top title
	hist->SetTitleFont(132);         // Font for X, Y, and Top title
	//hist->GetXaxis()->SetNdivisions(505);
	//hist->GetYaxis()->SetNdivisions(505);
	//hist->GetZaxis()->SetNdivisions(505);
	hist->GetXaxis()->SetNdivisions(509);
	hist->GetYaxis()->SetNdivisions(509);
	hist->GetZaxis()->SetNdivisions(509);

	hist->SetMarkerSize(0.8);
	hist->GetXaxis()->SetLabelFont(132);     // Font for X label
	hist->GetYaxis()->SetLabelFont(132);     // Font for Y label
	hist->GetZaxis()->SetLabelFont(132);     // Font for Y label
	hist->GetXaxis()->SetTitleFont(132);     // Font for X title 
	hist->GetYaxis()->SetTitleFont(132);     // Font for X title 
	hist->GetZaxis()->SetTitleFont(132);     // Font for X title  ///  Times new Luoma 22 bold
	hist->GetXaxis()->SetLabelSize(0.05);   // Size for X label
	hist->GetYaxis()->SetLabelSize(0.05);   // Size for Y label
	hist->GetZaxis()->SetLabelSize(0.05);   // Size for Y label
	hist->GetXaxis()->SetLabelOffset(0.015);   // Size for X label
	hist->GetYaxis()->SetLabelOffset(0.015);   // Size for Y label
	hist->GetZaxis()->SetLabelOffset(0.015);   // Size for Y label
	hist->GetXaxis()->SetTitleOffset(1.2);     // X title offset
	hist->GetYaxis()->SetTitleOffset(1.2);     // Y title offset
	hist->GetZaxis()->SetTitleOffset(1.);     // Y title offset
	hist->GetXaxis()->SetTitleSize(0.06);      // X title size
	hist->GetYaxis()->SetTitleSize(0.06);      // Y title size
	hist->GetZaxis()->SetTitleSize(0.06);      // Y title size
}

void setTLegend(TLegend*legend){
				legend->SetFillStyle(0);
				legend->SetTextFont(132);
				//legend->SetTextSize(0.05099067);
				legend->SetNColumns(1);
				legend->SetBorderSize(0);
				legend->SetFillColor(0);
}

void setRooPlotStyle(RooPlot* frame){
	frame->GetXaxis()->SetNdivisions(505);
	frame->GetYaxis()->SetNdivisions(505);
	frame->GetXaxis()->SetLabelFont(22);
	frame->GetYaxis()->SetLabelFont(22);
	frame->GetXaxis()->SetTitleFont(22);
	frame->GetYaxis()->SetTitleFont(22);
	frame->GetXaxis()->SetLabelSize(0.06);
	frame->GetYaxis()->SetLabelSize(0.05);
	frame->GetXaxis()->SetLabelOffset(0.015);
	frame->GetYaxis()->SetLabelOffset(0.015);
	frame->GetXaxis()->SetTitleOffset(1.2);
	frame->GetYaxis()->SetTitleOffset(1.);
	frame->GetXaxis()->SetTitleSize(0.06);
	frame->GetYaxis()->SetTitleSize(0.06);
	frame->SetTitle("");
}

void setStack(THStack* stack){
	stack->GetXaxis()->SetNdivisions(509);
	stack->GetYaxis()->SetNdivisions(509);
	//stack->GetXaxis()->SetLabelFont(22);
	//stack->GetYaxis()->SetLabelFont(22);
	//stack->GetXaxis()->SetTitleFont(22); 
	//stack->GetYaxis()->SetTitleFont(22); 
	stack->GetXaxis()->SetLabelFont(132);
	stack->GetYaxis()->SetLabelFont(132);
	stack->GetXaxis()->SetTitleFont(132); 
	stack->GetYaxis()->SetTitleFont(132); 
	stack->GetXaxis()->SetLabelSize(0.06);
	stack->GetYaxis()->SetLabelSize(0.06);
	//stack->GetXaxis()->CenterTitle(true);
	//stack->GetYaxis()->CenterTitle(true);
	stack->GetXaxis()->SetLabelOffset(0.015);
	stack->GetYaxis()->SetLabelOffset(0.015);
	stack->GetXaxis()->SetTitleOffset(1.);
	stack->GetYaxis()->SetTitleOffset(1.);
	stack->GetXaxis()->SetTitleSize(0.08);
	stack->GetYaxis()->SetTitleSize(0.08);
}

void DrawText(double x, double y, TString s, double size = 0.05, Color_t color = kBlack, bool rotated = false)
{
    TLatex *ltx = new TLatex();
    ltx->SetNDC(kTRUE);
    ltx->SetTextColor(color);
    ltx->SetTextFont(42);
    ltx->SetTextSize(size);
    if(rotated)
    {
        ltx -> SetTextAlign(22);
        ltx -> SetTextAngle(90);
    }
    ltx->DrawLatex(x,y,s.Data());

}

void setTGraphErrors(TGraphErrors* hist){
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
	gStyle->SetFillColor(0);
	//hist->SetTitleFont(22);         // Font for X, Y, and Top title
	hist->GetXaxis()->SetNdivisions(505);
	hist->GetYaxis()->SetNdivisions(505);

	hist->SetMarkerSize(1.0);
	hist->SetMarkerStyle(20);
	hist->GetXaxis()->SetLabelFont(132);     // Font for X label
	hist->GetYaxis()->SetLabelFont(132);     // Font for Y label
	hist->GetXaxis()->SetTitleFont(132);     // Font for X title 
	hist->GetYaxis()->SetTitleFont(132);     // Font for X title 
	hist->GetXaxis()->SetLabelSize(0.06);   // Size for X label
	hist->GetYaxis()->SetLabelSize(0.05);   // Size for Y label
	hist->GetXaxis()->SetLabelOffset(0.015);   // Size for X label
	hist->GetYaxis()->SetLabelOffset(0.020);   // Size for Y label
	hist->GetXaxis()->SetTitleOffset(1.2);     // X title offset
	hist->GetYaxis()->SetTitleOffset(1.2);     // Y title offset
	hist->GetXaxis()->SetTitleSize(0.06);      // X title size
	hist->GetYaxis()->SetTitleSize(0.06);      // Y title size
}

void setTGraph(TGraph* hist){

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
	hist->GetXaxis()->SetNdivisions(510);
	hist->GetYaxis()->SetNdivisions(510);

	hist->SetMarkerSize(1.0);
	hist->SetMarkerStyle(20);
	hist->GetXaxis()->SetLabelFont(132);     // Font for X label
	hist->GetYaxis()->SetLabelFont(132);     // Font for Y label
	hist->GetXaxis()->SetTitleFont(132);     // Font for X title 
	hist->GetYaxis()->SetTitleFont(132);     // Font for X title 
	hist->GetXaxis()->SetLabelSize(0.05);   // Size for X label
	hist->GetYaxis()->SetLabelSize(0.05);   // Size for Y label
	hist->GetXaxis()->SetLabelOffset(0.015);   // Size for X label
	hist->GetYaxis()->SetLabelOffset(0.020);   // Size for Y label
	hist->GetXaxis()->SetTitleOffset(1.2);     // X title offset
	hist->GetYaxis()->SetTitleOffset(1.2);     // Y title offset
	hist->GetXaxis()->SetTitleSize(0.06);      // X title size
	hist->GetYaxis()->SetTitleSize(0.06);      // Y title size
}

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TLorentzVector.h"
#include "TSystem.h"
#include "iostream"
#include <iomanip>
#include "TString.h"
#include "TCut.h"
#include "TChain.h"
#include "/lzufs/user/wanghj/LHCb/Lc2ppipi/snakemake0/Snake/cut.h"

using namespace std;
typedef std::vector<int> Vint;
typedef std::vector<double> Vdouble;

void idmatch_LcK(TString RootChainIN, TString RootChainOUT){
		TChain* chain = new TChain("LbToLcPi_LcTopPPiPi/DecayTree");
		chain->Add(RootChainIN);
		TFile* file_chain = new TFile(RootChainOUT,"recreate");
		cout<<"input : "<<RootChainIN<<endl;
		cout<<"outfile : "<<RootChainOUT<<endl;
		cout<<"cut : "<<cut_MC_truth_matched_k.GetTitle()<<endl<<endl;
		TTree *recchain = chain->CopyTree(cut_MC_truth_matched_k);
		recchain->Write();
		file_chain->Close();
}

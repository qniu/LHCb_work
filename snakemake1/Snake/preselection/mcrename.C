#include "TCut.h"
#include <string>
#include <ROOT/RDataFrame.hxx>
using namespace std;

int mcrename(string infile, string outfile){
		string treename = "DecayTree";
		ROOT::RDataFrame myDF(treename, infile);
		if(infile.find("") != std::string::npos){
				auto newDF = myDF
						.Define(     "pp_ProbNNk_old",     "pp_ProbNNk") .Redefine(     "pp_ProbNNk",     "pp_ProbNNk_pidcorr_default")
						.Define(    "pp_ProbNNpi_old",    "pp_ProbNNpi") .Redefine(    "pp_ProbNNpi",    "pp_ProbNNpi_pidcorr_default")
						.Define(     "pp_ProbNNp_old",     "pp_ProbNNp") .Redefine(     "pp_ProbNNp",     "pp_ProbNNp_pidcorr_default")
						.Define(    "pip_ProbNNk_old",    "pip_ProbNNk") .Redefine(    "pip_ProbNNk",    "pip_ProbNNk_pidcorr_default")
						.Define(   "pip_ProbNNpi_old",   "pip_ProbNNpi") .Redefine(   "pip_ProbNNpi",   "pip_ProbNNpi_pidcorr_default")
						.Define(    "pip_ProbNNp_old",    "pip_ProbNNp") .Redefine(    "pip_ProbNNp",    "pip_ProbNNp_pidcorr_default")
						.Define(    "pim_ProbNNk_old",    "pim_ProbNNk") .Redefine(    "pim_ProbNNk",    "pim_ProbNNk_pidcorr_default")
						.Define(   "pim_ProbNNpi_old",   "pim_ProbNNpi") .Redefine(   "pim_ProbNNpi",   "pim_ProbNNpi_pidcorr_default")
						.Define(    "pim_ProbNNp_old",    "pim_ProbNNp") .Redefine(    "pim_ProbNNp",    "pim_ProbNNp_pidcorr_default")
						.Define( "Lb_pim_ProbNNk_old", "Lb_pim_ProbNNk") .Redefine( "Lb_pim_ProbNNk", "Lb_pim_ProbNNk_pidcorr_default")
						.Define("Lb_pim_ProbNNpi_old","Lb_pim_ProbNNpi") .Redefine("Lb_pim_ProbNNpi","Lb_pim_ProbNNpi_pidcorr_default")
						.Define( "Lb_pim_ProbNNp_old", "Lb_pim_ProbNNp") .Redefine( "Lb_pim_ProbNNp", "Lb_pim_ProbNNp_pidcorr_default")
						.Snapshot(treename, outfile);
		}
		return 0;
}

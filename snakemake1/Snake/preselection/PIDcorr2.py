##############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import sys
import re

def config_Lcpi(kernels):
    track_Lcpi = {
        'Lb_pim': {
            "ProbNNk": ("ProbNNk", "pi_Dstar2Dpi","MC15TuneV1_ProbNNK", kernels),
            "ProbNNpi":("ProbNNpi","pi_Dstar2Dpi","MC15TuneV1_ProbNNpi", kernels),
            "ProbNNp": ("ProbNNp", "pi_Dstar2Dpi","MC15TuneV1_ProbNNp", kernels),
        },
    }
    return track_Lcpi

def config_LcK(kernels):
    track_LcK = {
        'Lb_pim': {
            "ProbNNk": ("ProbNNk", "pi_Dstar2Dpi", "MC15TuneV1_ProbNNK", kernels),
            "ProbNNpi":("ProbNNpi","pi_Dstar2Dpi", "MC15TuneV1_ProbNNpi", kernels),
            "ProbNNp": ("ProbNNp", "pi_Dstar2Dpi", "MC15TuneV1_ProbNNp", kernels),
        },
    }
    return track_LcK

def myPIDcorr2(infile, outfile, trackscale):

  pattern = r'(\d{4})_([A-Z]+)'
  match = re.search(pattern, infile)
  year = match.group(1)
  polarity = match.group(2)
  polarities = {"MU": "MagUp", "MD": "MagDown"}
  dataset = polarities[polarity] + "_" + year

  kernels = [
    ('default',    [0, 1, 2, 3]),   # Default kernel width, no template sampling 
                                    # and three "bootstrapped" templates with seeds 1..3
    ('syst1',      [0]),            # Wider "syst1" kernel, no template sampling 
  ]

  config = {
     'pp': {
         "ProbNNk": ("ProbNNk", "p_Lb2Lcpi","Brunel_MC15TuneV1_ProbNNK", kernels),
         "ProbNNpi":("ProbNNpi","p_Lb2Lcpi","Brunel_MC15TuneV1_ProbNNpi", kernels),
         "ProbNNp": ("ProbNNp", "p_Lb2Lcpi","Brunel_MC15TuneV1_ProbNNp", kernels),
     },
     'pip': {
         "ProbNNk": ("ProbNNk", "pi_Dstar2Dpi", "MC15TuneV1_ProbNNK", kernels),
         "ProbNNpi":("ProbNNpi","pi_Dstar2Dpi", "MC15TuneV1_ProbNNpi", kernels),
         "ProbNNp": ("ProbNNp", "pi_Dstar2Dpi", "MC15TuneV1_ProbNNp", kernels),
     },
     'pim': {
         "ProbNNk": ("ProbNNk", "pi_Dstar2Dpi", "MC15TuneV1_ProbNNK", kernels),
         "ProbNNpi":("ProbNNpi","pi_Dstar2Dpi", "MC15TuneV1_ProbNNpi", kernels),
         "ProbNNp": ("ProbNNp", "pi_Dstar2Dpi", "MC15TuneV1_ProbNNp", kernels),
     },
  }

  if "Lcpi" in infile:
      # Name of the input tree
      config.update(config_Lcpi(kernels))
  elif "LcK" in infile:
      # Name of the input tree
      config.update(config_LcK(kernels))
      
  print(config)

  # Name of the input tree
  # Could also include ROOT directory, e.g. "Dir/Tree"
  input_tree = "DecayTree"
  
  # Postfixes of the Pt, Eta and Ntracks variables (ntuple variable name the particle name part)
  # e.g. if the ntuple contains "pi_PT" for the particle "pi", it should be just "PT"
  ptvar  = "PT"
  etavar = "ETA"
  pvar   = None
  ## Could also use P variable instead of eta
  #etavar = None
  #pvar   = "p"
  
  ntrvar = "nTracks"   # Number of tracks variable, this should correspond to the number of "Best tracks", not "Long tracks"!
  simversion = "Sim09" # Simulation version of the input MC file
  
  friend = False       # If True, write friend trees with resampled PID instead of copying the whole tree
  
  # List of (kernel, seeds) combinations for raw template smearing

  scale = (1, 1, float(trackscale))
  output_tree = input_tree.split("/")[-1]
  treename = input_tree

  from pidgen2.correct import correct

  input_location = f"{infile}:{input_tree}"
  
  for track, subst in config.items() :
    for var, (pidvar, sample, calibvar, kernel_list) in subst.items() :
  
      # Create the list of input branches, depending on whether Eta or P variable is available
      if pvar is None : 
        branches = f"{track}_{pidvar}:{track}_{ptvar}:{track}_{etavar}:{ntrvar}"
        eta_from_p = False
      else : 
        branches = f"{track}_{pidvar}:{track}_{ptvar}:{track}_{pvar}:{ntrvar}"
        eta_from_p = True
 
      outfile = outfile.replace('.root','')
 
      if friend : 
         output_root_file = f"{outfile}_{track}_{var}.root"
      else : 
         output_root_file = f"{outfile}.root"
  
      # Run resampling of a single variable in a single file
      correct(
         input = input_location,    # Input tuple
         simversion = simversion,   # Simulation version of the input MC file
         sample = sample,           # Calibration sample (e.g. "pi_Dstar2Dpi")
         dataset = dataset,         # Dataset (e.g. "MagUp_2016")
         variable = calibvar,       # Calibration variable (e.g. "MC15TuneV1_ProbNNK")
         branches = branches,       # List of resampling branches (typically, corresponding to Pt, Eta and Ntracks, e.g. "pt:eta:ntr")
         output = output_root_file, # Output ROOT file name
         outtree = output_tree,     # Output tree name
         plot = False,               # If template needs to be created from scratch, produce control plots
         pidcorr = f"{track}_{var}_pidcorr",  # Name of the corrected PID branch
         stat = f"{track}_{var}_pidstat",     # Name of output branch with calibration statistics for each resampled event
         mcstat = f"{track}_{var}_pidmcstat", # Name of output branch with MC statistics for each resampled event
         kernels = kernel_list,     # List of kernels and template seeds
         verbose = False,           # Print debugging information
         eta_from_p = eta_from_p,   # If eta needs to be calculated from p and pt
         friend = friend,           # If True, write output to friend trees
         library = "ak",            # Library to handle ROOT files with uproot if friend=False, can be 
                                    # "ak" (Awkward array), "np" (numpy) or "pd" (Pandas)
         step_size = 50000,         # Chunk size when writing files with uproot if friend=False
         nan = -1000.,              # Numerical value to substitute NaN, for regions w/o calibration data 
         scale = scale,             # Scale factors for input data # List of scale factors for each dimension of input data, to increase track multiplicity by 15%
         local_storage = "./templates/"+dataset+"/",  # Directory for local template storage, used when the template is not available in 
                                                      # the global storage on EOS. 
         local_mc_storage = "./mc_templates/"+dataset+"/",  # Directory for local template storage, used when the template is not available in 
                                                            # the global storage on EOS. 
      )
  
      if not friend : 
        # All subsequent calls use output file as input
        input_location = f"{output_root_file}:{output_tree}"

if __name__ == '__main__':
    myPIDcorr2(sys.argv[1], sys.argv[2], sys.argv[3])


#pragma once

#include "TCut.h"

TCut cut_MC_truth_matched_pi = \
" abs(Lb_TRUEID)==5122 && abs(Pim_TRUEID)==211 && abs(Lc_TRUEID)==4122 && abs(Pp_TRUEID)==2212 && abs(Kp_TRUEID)==321 && abs(Km_TRUEID)==321"\
" && abs(Pim_MC_MOTHER_ID)==5122 && abs(Lc_MC_MOTHER_ID)==5122 && abs(Pp_MC_MOTHER_ID)==4122 && abs(Kp_MC_MOTHER_ID)==4122 && abs(Km_MC_MOTHER_ID)==4122";

TCut cut_MC_truth_matched_k = \
" abs(Lb_TRUEID)==5122 && abs(Pim_TRUEID)==321 && abs(Lc_TRUEID)==4122 && abs(Pp_TRUEID)==2212 && abs(Kp_TRUEID)==321 && abs(Km_TRUEID)==321"\
" && abs(Pim_MC_MOTHER_ID)==5122 && abs(Lc_MC_MOTHER_ID)==5122 && abs(Pp_MC_MOTHER_ID)==4122 && abs(Kp_MC_MOTHER_ID)==4122 && abs(Km_MC_MOTHER_ID)==4122";

TCut cut_L0 = "Lb_L0HadronDecision_TOS || Lb_L0Global_TIS";
TCut cut_Hlt1 = "Lb_Hlt1TrackMVADecision_TOS || Lb_Hlt1TwoTrackMVADecision_TOS";
TCut cut_Hlt2 = "Lb_Hlt2Topo2BodyDecision_TOS || Lb_Hlt2Topo3BodyDecision_TOS || Lb_Hlt2Topo4BodyDecision_TOS";
TCut cut_trigger = cut_L0 + cut_Hlt1 + cut_Hlt2;

TCut cut_PID = \
 " Pim_ProbNNpi *(1.0 - Pim_ProbNNk) * (1.0 - Pim_ProbNNp)> 0.05"\
 " && Pp_ProbNNp *(1.0 - Pp_ProbNNk) * (1.0 - Pp_ProbNNpi) > 0.05"\
 " && Kp_ProbNNk *(1.0 - Kp_ProbNNpi) * (1.0 - Kp_ProbNNp) > 0.05"\
 " && Km_ProbNNk *(1.0 - Km_ProbNNpi) * (1.0 - Km_ProbNNp) > 0.05"\
 " && Pim_isMuon==0";

TCut cut_Lc = "DTFonlyPV_Lc_M > 2260 && DTFonlyPV_Lc_M < 2310";
TCut cut_Lc_sideband = "DTFonlyPV_Lc_M>2320&&DTFonlyPV_Lc_M<2370";
TCut cut_loose_Lb = "DTFonlyPV_Lb_M > 5520 && DTFonlyPV_Lb_M < 5800";//loose cut
TCut cut_Lb_sw = "DTFonlyPV_Lb_M > 5520 && DTFonlyPV_Lb_M < 5720";
TCut cut_signal = "DTFonlyPV_Lb_M > 5550 && DTFonlyPV_Lb_M < 5700";
TCut cut_sideband = "DTFonlyPV_Lb_M > 5750 && DTFonlyPV_Lb_M < 6200";


TCut cut_PID_final_2016 = "BDT>-0.09"\
  " &&Pim_ProbNNpi *(1.0 - Pim_ProbNNk) * (1.0 - Pim_ProbNNp)> 0.07"\
  " && Pp_ProbNNp *(1.0 - Pp_ProbNNk) * (1.0 - Pp_ProbNNpi) > 0.25"\
  " && Kp_ProbNNk *(1.0 - Kp_ProbNNpi) * (1.0 - Kp_ProbNNp) > 0.1"\
  " && Km_ProbNNk *(1.0 - Km_ProbNNpi) * (1.0 - Km_ProbNNp) > 0.1";
TCut cut_PID_final_2017 = "BDT>-0.01"\
  " &&Pim_ProbNNpi *(1.0 - Pim_ProbNNk) * (1.0 - Pim_ProbNNp)> 0.07"\
  " && Pp_ProbNNp *(1.0 - Pp_ProbNNk) * (1.0 - Pp_ProbNNpi) > 0.24"\
  " && Kp_ProbNNk *(1.0 - Kp_ProbNNpi) * (1.0 - Kp_ProbNNp) > 0.07"\
  " && Km_ProbNNk *(1.0 - Km_ProbNNpi) * (1.0 - Km_ProbNNp) > 0.07";
TCut cut_PID_final_2018 = "BDT>-0.12"\
  " &&Pim_ProbNNpi *(1.0 - Pim_ProbNNk) * (1.0 - Pim_ProbNNp)> 0.06"\
  " && Pp_ProbNNp *(1.0 - Pp_ProbNNk) * (1.0 - Pp_ProbNNpi) > 0.28"\
  " && Kp_ProbNNk *(1.0 - Kp_ProbNNpi) * (1.0 - Kp_ProbNNp) > 0.09"\
  " && Km_ProbNNk *(1.0 - Km_ProbNNpi) * (1.0 - Km_ProbNNp) > 0.09";


//Unused

TCut cut_rm_misLc = "DTFonlyPV_Lc_M > 2240";//remove misLc


